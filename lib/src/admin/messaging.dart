part of '../../admin.dart';

/// Represents the Android-specific options
class AndroidConfig
    extends JsObjectWrapper<messaging_interop.AndroidConfigJsImpl>
    implements _AndroidConfig {
  static final _expando = Expando<AndroidConfig>();

  /// Represents the Android-specific options
  ///
  /// `collapseKey`:
  /// Collapse key for the message. Collapse key serves as an identifier for a
  /// group of messages that can be collapsed, so that only the last message
  /// gets sent when delivery can be resumed. A maximum of four different
  /// collapse keys may be active at any given time.
  ///
  /// `data`:
  /// A collection of data fields to be included in the message. All values
  /// must be strings.
  ///
  /// `fcmOptions`:
  /// Options for features provided by the FCM SDK for Android.
  ///
  /// `notification`
  /// Android notification to be included in the message.
  ///
  /// `priority`:
  /// Priority of the message. Must be either normal or high.
  ///
  /// `restrictedPackageName`:
  /// Package name of the application where the registration tokens must match
  /// in order to receive the message.
  ///
  /// `ttl`:
  /// Time-to-live duration of the message in milliseconds.
  ///
  /// See: https://firebase.google.com/docs/reference/admin/node/admin.messaging.AndroidConfig
  factory AndroidConfig({
    String collapseKey,
    Map<String, String> data,
    messaging_interop.AndroidFcmOptions fcmOptions,
    AndroidNotification notification,
    String priority,
    String restrictedPackageName,
    num ttl,
  }) =>
      AndroidConfig._fromJsObject(messaging_interop.AndroidConfigJsImpl(
        collapseKey: collapseKey,
        data: jsify(data),
        fcmOptions: fcmOptions,
        notification: notification.jsObject,
        priority: priority,
        restrictedPackageName: restrictedPackageName,
        ttl: ttl,
      ));

  AndroidConfig._fromJsObject(messaging_interop.AndroidConfigJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  String get collapseKey => jsObject.collapseKey;

  @override
  set collapseKey(String v) => jsObject.collapseKey = v;

  @override
  Map<String, String> get data {
    final result = dartify(jsObject.data);
    if (result == null) return null;
    return (result as Map<String, dynamic>).cast<String, String>();
  }

  @override
  set data(Map<String, String> v) => jsObject.data = jsify(v);

  @override
  messaging_interop.AndroidFcmOptions get fcmOptions => jsObject.fcmOptions;

  @override
  set fcmOptions(messaging_interop.AndroidFcmOptions v) =>
      jsObject.fcmOptions = v;

  @override
  AndroidNotification get notification =>
      AndroidNotification.getInstance(jsObject.notification);

  @override
  set notification(AndroidNotification v) => jsObject.notification = v.jsObject;

  @override
  String get priority => jsObject.priority;

  @override
  set priority(String v) => jsObject.priority = v;

  @override
  String get restrictedPackageName => jsObject.restrictedPackageName;

  @override
  set restrictedPackageName(String v) => jsObject.restrictedPackageName = v;

  @override
  num get ttl => jsObject.ttl;

  @override
  set ttl(num v) => jsObject.ttl = v;

  /// Get instance of [AndroidConfig]
  static AndroidConfig getInstance(
      messaging_interop.AndroidConfigJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= AndroidConfig._fromJsObject(jsObject);
  }
}

/// Represents the Android-specific notification options that can be included
/// in [AndroidConfig]
class AndroidNotification
    extends JsObjectWrapper<messaging_interop.AndroidNotificationJsImpl>
    implements _AndroidNotification {
  static final _expando = Expando<AndroidNotification>();

  /// Represents the Android-specific notification options that can be included
  /// in [AndroidConfig]
  ///
  /// See: https://firebase.google.com/docs/reference/admin/node/admin.messaging.AndroidNotification
  ///
  /// `body`:
  /// Body of the Android notification.
  ///
  /// `bodyLockArgs`:
  /// An array of resource keys that will be used in place of the format
  /// specifiers in bodyLocKey.
  ///
  /// `bodyLockKey`:
  /// Key of the body string in the app's string resource to use to localize
  /// the body text.
  ///
  /// `channelId`:
  /// The Android notification channel ID (new in Android O). The app must
  /// create a channel with this channel ID before any notification with this
  /// channel ID can be received. If you don't send this channel ID in the
  /// request, or if the channel ID provided has not yet been created by the
  /// app, FCM uses the channel ID specified in the app manifest.
  ///
  /// `clickAction`:
  /// Action associated with a user click on the notification. If specified, an
  /// activity with a matching Intent Filter is launched when a user clicks on
  /// the notification.
  ///
  /// `color`:
  /// Notification icon color in #rrggbb format.
  ///
  /// `defaultLightSettings`
  /// If set to true, use the Android framework's default LED light settings
  /// for the notification. Default values are specified in config.xml. If
  /// default_light_settings is set to true and light_settings is also set, the
  /// user-specified light_settings is used instead of the default value.
  ///
  /// `defaultSound`:
  /// If set to true, use the Android framework's default sound for the
  /// notification. Default values are specified in config.xml.
  ///
  /// `defaultVibrateTimings`:
  /// If set to true, use the Android framework's default vibrate pattern for
  /// the notification. Default values are specified in config.xml. If
  /// default_vibrate_timings is set to true and vibrate_timings is also set,
  /// the default value is used instead of the user-specified vibrate_timings.
  ///
  /// `eventTimestamp`:
  /// For notifications that inform users about events with an absolute time
  /// reference, sets the time that the event in the notification occurred.
  /// Notifications in the panel are sorted by this time.
  ///
  /// `icon`:
  /// Icon resource for the Android notification.
  ///
  /// `imageUrl`:
  /// URL of an image to be displayed in the notification.
  ///
  /// `lightSettings`:
  /// Settings to control the notification's LED blinking rate and color if LED
  /// is available on the device. The total blinking time is controlled by the
  /// OS.
  ///
  /// `localOnly`:
  /// Sets whether or not this notification is relevant only to the current
  /// device. Some notifications can be bridged to other devices for remote
  /// display, such as a Wear OS watch. This hint can be set to recommend this
  /// notification not be bridged. See Wear OS guides
  ///
  /// `notificationCount`:
  /// Sets the number of items this notification represents. May be displayed
  /// as a badge count for Launchers that support badging. See
  /// [NotificationBadge(https:///developer.android.com/training/notify-user/
  /// badges). For example, this might be useful if you're using just one
  /// notification to represent multiple new messages but you want the count
  /// here to represent the number of total new messages. If zero or
  /// unspecified, systems that support badging use the default, which is to \
  /// increment a number displayed on the long-press menu each time a new
  /// notification arrives.
  ///
  /// `priority`:
  /// undefined | "high" | "min" | "low" | "default" | "max".
  /// Sets the relative priority for this notification. Low-priority
  /// notifications may be hidden from the user in certain situations. Note
  /// this priority differs from AndroidMessagePriority. This priority is
  /// processed by the client after the message has been delivered. Whereas
  /// AndroidMessagePriority is an FCM concept that controls when the message
  /// is delivered.

  /// `sound`:
  /// File name of the sound to be played when the device receives the
  /// notification.
  ///
  /// `sticky`:
  /// When set to false or unset, the notification is automatically dismissed
  /// when the user clicks it in the panel. When set to true, the notification
  /// persists even when the user clicks it.
  ///
  /// `tag`:
  /// Notification tag. This is an identifier used to replace existing
  /// notifications in the notification drawer. If not specified, each request
  /// creates a new notification.
  ///
  /// `ticker`:
  /// Sets the "ticker" text, which is sent to accessibility services. Prior to
  /// API level 21 (Lollipop), sets the text that is displayed in the status
  /// bar when the notification first arrives.
  ///
  /// `title`
  /// Title of the Android notification. When provided, overrides the title set
  /// via admin.messaging.Notification.
  ///
  /// `titleLocArgs`
  /// An array of resource keys that will be used in place of the format
  /// specifiers in titleLocKey.
  ///
  /// `titleLocKey`:
  /// Key of the title string in the app's string resource to use to localize
  /// the title text.
  ///
  /// `vibrateTimingsMillis`
  /// Sets the vibration pattern to use. Pass in an array of milliseconds to
  /// turn the vibrator on or off. The first value indicates the duration to
  /// wait before turning the vibrator on. The next value indicates the
  /// duration to keep the vibrator on. Subsequent values alternate between
  /// duration to turn the vibrator off and to turn the vibrator on. If
  /// vibrate_timings is set and default_vibrate_timings is set to true, the
  /// default value is used instead of the user-specified vibrate_timings.
  ///
  /// `visibility`:
  /// visibility: "private" | "public" | "secret"
  /// Sets the visibility of the notification. Must be either private, public,
  /// or secret. If unspecified, defaults to private.
  factory AndroidNotification({
    String body,
    String bodyLocArgs,
    String bodyLocKey,
    String channelId,
    String clickAction,
    String color,
    bool defaultLightSettings,
    bool defaultSound,
    bool defaultVibrateTimings,
    DateTime eventTimestamp,
    String icon,
    String imageUrl,
    messaging_interop.LightSettings lightSettings,
    bool localOnly,
    num notificationCount,
    String priority,
    String sound,
    bool sticky,
    String tag,
    String ticker,
    String title,
    String titleLocArgs,
    String titleLocKey,
    List<num> vibrateTimingsMillis,
    String visibility = 'secret',
  }) =>
      AndroidNotification._fromJsObject(
          messaging_interop.AndroidNotificationJsImpl(
        body: body,
        bodyLocArgs: bodyLocArgs,
        bodyLocKey: bodyLocKey,
        channelId: channelId,
        clickAction: clickAction,
        color: color,
        defaultLightSettings: defaultLightSettings,
        defaultSound: defaultSound,
        defaultVibrateTimings: defaultVibrateTimings,
        eventTimestamp: jsify(eventTimestamp),
        icon: icon,
        imageUrl: imageUrl,
        lightSettings: lightSettings,
        localOnly: localOnly,
        notificationCount: notificationCount,
        priority: priority,
        sound: sound,
        sticky: sticky,
        tag: tag,
        ticker: ticker,
        title: title,
        titleLocArgs: titleLocArgs,
        titleLocKey: titleLocKey,
        vibrateTimingsMillis: jsify(vibrateTimingsMillis),
        visibility: visibility,
      ));

  AndroidNotification._fromJsObject(
      messaging_interop.AndroidNotificationJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  String get body => jsObject.body;
  @override
  set body(String _body) => jsObject.body = _body;
  @override
  String get bodyLocArgs => jsObject.bodyLocArgs;
  @override
  set bodyLocArgs(String _bodyLocArgs) => jsObject.bodyLocArgs = _bodyLocArgs;
  @override
  String get bodyLocKey => jsObject.bodyLocKey;
  @override
  set bodyLocKey(String _bodyLocKey) => jsObject.bodyLocKey = _bodyLocKey;
  @override
  String get channelId => jsObject.channelId;
  @override
  set channelId(String _channelId) => jsObject.channelId = _channelId;
  @override
  String get clickAction => jsObject.clickAction;
  @override
  set clickAction(String _clickAction) => jsObject.clickAction = _clickAction;
  @override
  String get color => jsObject.color;
  @override
  set color(String _color) => jsObject.color = _color;
  @override
  bool get defaultLightSettings => jsObject.defaultLightSettings;
  @override
  set defaultLightSettings(bool _defaultLightSettings) =>
      jsObject.defaultLightSettings = _defaultLightSettings;
  @override
  bool get defaultSound => jsObject.defaultSound;
  @override
  set defaultSound(bool _defaultSound) => jsObject.defaultSound = _defaultSound;
  @override
  bool get defaultVibrateTimings => jsObject.defaultVibrateTimings;
  @override
  set defaultVibrateTimings(bool _defaultVibrateTimings) =>
      jsObject.defaultVibrateTimings = _defaultVibrateTimings;
  @override
  DateTime get eventTimestamp => jsObject.eventTimestamp;
  @override
  set eventTimestamp(DateTime _eventTimestamp) =>
      jsObject.eventTimestamp = jsify(_eventTimestamp);
  @override
  String get icon => jsObject.icon;
  @override
  set icon(String _icon) => jsObject.icon = _icon;
  @override
  String get imageUrl => jsObject.imageUrl;
  @override
  set imageUrl(String _imageUrl) => jsObject.imageUrl = _imageUrl;
  @override
  messaging_interop.LightSettings get lightSettings => jsObject.lightSettings;
  @override
  set lightSettings(messaging_interop.LightSettings _lightSettings) =>
      jsObject.lightSettings = _lightSettings;
  @override
  bool get localOnly => jsObject.localOnly;
  @override
  set localOnly(bool _localOnly) => jsObject.localOnly = _localOnly;
  @override
  num get notificationCount => jsObject.notificationCount;
  @override
  set notificationCount(num _notificationCount) =>
      jsObject.notificationCount = _notificationCount;
  @override
  String get priority => jsObject.priority;
  @override
  set priority(String _priority) => jsObject.priority = _priority;
  @override
  String get sound => jsObject.sound;
  @override
  set sound(String _sound) => jsObject.sound = _sound;
  @override
  bool get sticky => jsObject.sticky;
  @override
  set sticky(bool _sticky) => jsObject.sticky = _sticky;
  @override
  String get tag => jsObject.tag;
  @override
  set tag(String _tag) => jsObject.tag = _tag;
  @override
  String get ticker => jsObject.ticker;
  @override
  set ticker(String _ticker) => jsObject.ticker = _ticker;
  @override
  String get title => jsObject.title;
  @override
  set title(String _title) => jsObject.title = _title;
  @override
  String get titleLocArgs => jsObject.titleLocArgs;
  @override
  set titleLocArgs(String _titleLocArgs) =>
      jsObject.titleLocArgs = _titleLocArgs;
  @override
  String get titleLocKey => jsObject.titleLocKey;
  @override
  set titleLocKey(String _titleLocKey) => jsObject.titleLocKey = _titleLocKey;
  @override
  List<num> get vibrateTimingsMillis => jsObject.vibrateTimingsMillis == null
      ? null
      : List.from(jsObject.vibrateTimingsMillis);
  @override
  set vibrateTimingsMillis(List<num> _vibrateTimingsMillis) =>
      jsObject.vibrateTimingsMillis = jsify(_vibrateTimingsMillis);
  @override
  String get visibility => jsObject.visibility;
  @override
  set visibility(String _visibility) => jsObject.visibility = _visibility;

  /// Get instance of [AndroidNotification] from jsObject.
  static AndroidNotification getInstance(
      messaging_interop.AndroidNotificationJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= AndroidNotification._fromJsObject(jsObject);
  }
}

class ApnsConfig extends JsObjectWrapper<messaging_interop.ApnsConfigJsImpl>
    implements _ApnsConfig {
  static final _expando = Expando<ApnsConfig>();

  factory ApnsConfig({
    messaging_interop.ApnsFcmOptions fcmOptions,
    Map<String, String> headers,
    ApnsPayload payload,
  }) =>
      ApnsConfig._fromJsObject(messaging_interop.ApnsConfigJsImpl(
        fcmOptions: fcmOptions,
        headers: jsify(headers),
        payload: payload.jsObject,
      ));

  ApnsConfig._fromJsObject(messaging_interop.ApnsConfigJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  messaging_interop.ApnsFcmOptions get fcmOptions => jsObject.fcmOptions;

  @override
  set fcmOptions(messaging_interop.ApnsFcmOptions v) => jsObject.fcmOptions = v;

  @override
  Map<String, String> get headers {
    if (jsObject.headers == null) return null;
    final result = dartify(jsObject.headers);
    return (result as Map<String, dynamic>).cast<String, String>();
  }

  @override
  set headers(Map<String, String> v) => jsObject.headers = jsify(v);

  @override
  ApnsPayload get payload => ApnsPayload.getInstance(jsObject.payload);

  @override
  set payload(ApnsPayload v) => jsObject.payload = v.jsObject;

  static ApnsConfig getInstance(messaging_interop.ApnsConfigJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= ApnsConfig._fromJsObject(jsObject);
  }
}

/// Represents the payload of an APNs message. Mainly consists of the aps
/// dictionary. But may also contain other arbitrary custom keys.
class ApnsPayload extends JsObjectWrapper<messaging_interop.ApnsPayloadJsImpl> {
  static final _expando = Expando<ApnsPayload>();

  ApnsPayload._fromJsObject(messaging_interop.ApnsPayloadJsImpl jsObject)
      : super.fromJsObject(jsObject);

  /// The aps dictionary to be included in the message.
  Aps get aps => Aps.getInstance(jsObject.aps);

  /// Represents the payload of an APNs message. Mainly consists of the aps
  /// dictionary. But may also contain other arbitrary custom keys.
  Map<String, dynamic> get customData => dartify(jsObject.customData);

  /// Get instance of [ApnsPayload] from jsObject.
  static ApnsPayload getInstance(messaging_interop.ApnsPayloadJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= ApnsPayload._fromJsObject(jsObject);
  }
}

/// Represents the aps dictionary that is part of APNs messages.
class Aps extends JsObjectWrapper<messaging_interop.ApsJsImpl> {
  static final _expando = Expando<Aps>();

  Aps._fromJsObject(messaging_interop.ApsJsImpl jsObject)
      : super.fromJsObject(jsObject);

  /// Alert to be included in the message.
  /// can be  [String] | [ApsAlert]
  dynamic /*String | ApsAlert*/ get alert => jsObject.alert;

  /// Badge to be displayed with the message. Set to 0 to remove the badge.
  /// When not specified, the badge will remain unchanged.
  num get badge => jsObject.badge;

  /// Type of the notification.
  String get category => jsObject.category;

  /// Specifies whether to configure a background update notification.
  bool get contentAvailable => jsObject.contentAvailable;

  /// Represents the aps dictionary that is part of APNs messages.
  Map<String, dynamic> get customData => dartify(jsObject.customData);

  /// Specifies whether to set the mutable-content property on the message so
  /// the clients can modify the notification via app extensions.
  bool get mutableContent => jsObject.mutableContent;

  /// Sound to be played with the message.
  /// It can be  [String] | [CriticalSound]
  dynamic /*String | CriticalSound*/ get sound => jsObject.sound;

  /// An app-specific identifier for grouping notifications.
  String get threadId => jsObject.threadId;

  /// Get instance of [Aps] from jsObject.
  static Aps getInstance(messaging_interop.ApsJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= Aps._fromJsObject(jsObject);
  }
}

class ApsAlert extends JsObjectWrapper<messaging_interop.ApsAlertJsImpl> {
  static final _expando = Expando<ApsAlert>();

  ApsAlert._fromJsObject(messaging_interop.ApsAlertJsImpl jsObject)
      : super.fromJsObject(jsObject);

  String get actionLocKey => jsObject.actionLocKey;

  String get body => jsObject.body;

  String get launchImage => jsObject.launchImage;

  List<String> get locArgs => List.from(jsObject.locArgs);

  String get locKey => jsObject.locKey;

  String get subtitle => jsObject.subtitle;

  List<String> get subtitleLocArgs => List.from(jsObject.subtitleLocArgs);

  String get subtitleLocKey => jsObject.subtitleLocKey;

  String get title => jsObject.title;

  List<String> get titleLocArgs => List.from(jsObject.titleLocArgs);

  String get titleLocKey => jsObject.titleLocKey;

  static ApsAlert getInstance(messaging_interop.ApsAlertJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= ApsAlert._fromJsObject(jsObject);
  }
}

/// Interface representing the server response from the [Messaging.sendAll] and
/// [Messaging.sendMulticast()] methods.
class BatchResponse
    extends JsObjectWrapper<messaging_interop.BatchResponseJsImpl> {
  static final _expando = Expando<BatchResponse>();

  BatchResponse._fromJsObject(messaging_interop.BatchResponseJsImpl jsObject)
      : super.fromJsObject(jsObject);

  /// The number of messages that resulted in errors when sending.
  num get failureCount => jsObject.failureCount;

  /// An array of responses, each corresponding to a message.
  List<messaging_interop.SendResponse> get responses => jsObject.responses;

  /// The number of messages that were successfully handed off for sending.
  num get successCount => jsObject.successCount;

  /// Get instanfce of [BatchResponse] from jsObject.
  static BatchResponse getInstance(
      messaging_interop.BatchResponseJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= BatchResponse._fromJsObject(jsObject);
  }
}

class ConditionMessage
    extends JsObjectWrapper<messaging_interop.ConditionMessageJsImpl>
    implements _ConditionMessage {
  factory ConditionMessage({
    AndroidConfig android,
    ApnsConfig apns,
    String condition,
    Map<String, dynamic> data,
    messaging_interop.FcmOptions fcmOptions,
    messaging_interop.MessagingNotification notification,
    WebpushConfig webpush,
  }) =>
      ConditionMessage._fromJsObject(messaging_interop.ConditionMessageJsImpl(
        android: android.jsObject,
        apns: apns.jsObject,
        condition: condition,
        data: jsify(data),
        fcmOptions: fcmOptions,
        notification: notification,
        webpush: webpush.jsObject,
      ));

  ConditionMessage._fromJsObject(
      messaging_interop.ConditionMessageJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  AndroidConfig get android => AndroidConfig.getInstance(jsObject.android);

  @override
  set android(AndroidConfig v) => jsObject.android = v.jsObject;

  @override
  ApnsConfig get apns => ApnsConfig.getInstance(jsObject.apns);

  @override
  set apns(ApnsConfig v) => jsObject.apns = v.jsObject;

  @override
  String get condition => jsObject.condition;

  @override
  set condition(String v) => jsObject.condition = v;

  @override
  Map<String, dynamic> get data => dartify(jsObject.data);

  @override
  set data(Map<String, dynamic> v) => jsObject.data = jsify(v);

  @override
  messaging_interop.FcmOptions get fcmOptions => jsObject.fcmOptions;

  @override
  set fcmOptions(messaging_interop.FcmOptions v) => jsObject.fcmOptions = v;

  @override
  messaging_interop.MessagingNotification get notification =>
      jsObject.notification;

  @override
  set notification(messaging_interop.MessagingNotification v) =>
      jsObject.notification = v;

  @override
  WebpushConfig get webpush => WebpushConfig.getInstance(jsObject.webpush);

  @override
  set webpush(WebpushConfig v) => jsObject.webpush = v.jsObject;
}

/// Gets the Messaging service for the current app.
class Messaging extends JsObjectWrapper<messaging_interop.MessagingJsImpl>
    implements _Messaging {
  static final _expando = Expando<Messaging>();

  Messaging._fromJsObject(messaging_interop.MessagingJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  App get app => App.getInstance(jsObject.app);

  @override
  Future<String> send(dynamic message, {bool dryRun = false}) =>
      handleThenable(jsObject.send(message, dryRun));

  @override
  Future<BatchResponse> sendAll(List message, {bool dryRun = false}) =>
      handleThenable(jsObject.sendAll(message, dryRun))
          .then(BatchResponse.getInstance);

  @override
  Future<BatchResponse> sendMulticast(MulticastMessage message,
          {bool dryRun = false}) =>
      handleThenable(jsObject.sendMulticast(message.jsObject, dryRun))
          .then(BatchResponse.getInstance);

  @override
  Future<messaging_interop.MessagingConditionResponse> sendToCondition(
    String condition,
    MessagingPayload payload, [
    messaging_interop.MessagingOptions options,
  ]) {
    if (options == null) {
      return handleThenable(
          jsObject.sendToCondition(condition, payload.jsObject));
    } else {
      return handleThenable(
          jsObject.sendToCondition(condition, payload.jsObject, options));
    }
  }

  @override
  Future<messaging_interop.MessagingDevicesResponse> sendToDevice(
    List<String> registrationToken,
    MessagingPayload payload, [
    messaging_interop.MessagingOptions options,
  ]) {
    if (options == null) {
      return handleThenable(
          jsObject.sendToDevice(jsify(registrationToken), payload.jsObject));
    } else {
      return handleThenable(jsObject.sendToDevice(
          jsify(registrationToken), payload.jsObject, options));
    }
  }

  @override
  Future<MessagingDeviceGroupResponse> sendToDeviceGroup(
    String notificationKey,
    MessagingPayload payload, [
    messaging_interop.MessagingOptions options,
  ]) {
    if (options == null) {
      return handleThenable(
              jsObject.sendToDeviceGroup(notificationKey, payload.jsObject))
          .then(MessagingDeviceGroupResponse.getInstance);
    } else {
      return handleThenable(jsObject.sendToDeviceGroup(
              notificationKey, payload.jsObject, options))
          .then(MessagingDeviceGroupResponse.getInstance);
    }
  }

  @override
  Future<messaging_interop.MessagingTopicResponse> sendToTopic(
    String topic,
    MessagingPayload payload, [
    messaging_interop.MessagingOptions options,
  ]) {
    if (options == null) {
      return handleThenable(jsObject.sendToTopic(topic, payload.jsObject));
    } else {
      return handleThenable(
          jsObject.sendToTopic(topic, payload.jsObject, options));
    }
  }

  @override
  Future<MessagingTopicManagementResponse> subscribeToTopic(
          List<String> registrationToken, String topic) =>
      handleThenable(jsObject.subscribeToTopic(jsify(registrationToken), topic))
          .then(MessagingTopicManagementResponse.getInstance);

  @override
  Future<MessagingTopicManagementResponse> unsubscribeFromTopic(
          List<String> registrationToken, String topic) =>
      handleThenable(
              jsObject.unsubscribeFromTopic(jsify(registrationToken), topic))
          .then(MessagingTopicManagementResponse.getInstance);

  /// Get instance of [Messaging] from jsObject.
  static Messaging getInstance(messaging_interop.MessagingJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= Messaging._fromJsObject(jsObject);
  }
}

class MessagingDeviceGroupResponse extends JsObjectWrapper<
    messaging_interop.MessagingDeviceGroupResponseJsImpl> {
  static final _expando = Expando<MessagingDeviceGroupResponse>();

  MessagingDeviceGroupResponse._fromJsObject(
      messaging_interop.MessagingDeviceGroupResponseJsImpl jsObject)
      : super.fromJsObject(jsObject);

  List<String> get failedRegistrationTokens =>
      jsObject.failedRegistrationTokens;

  num get failureCount => jsObject.failureCount;

  num get successCount => jsObject.successCount;

  static MessagingDeviceGroupResponse getInstance(
      messaging_interop.MessagingDeviceGroupResponseJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??=
        MessagingDeviceGroupResponse._fromJsObject(jsObject);
  }
}

/// Interface representing a Firebase Cloud Messaging message payload.
class MessagingPayload
    extends JsObjectWrapper<messaging_interop.MessagingPayloadJsImpl>
    implements _MessagingPayload {
  /// Interface representing a Firebase Cloud Messaging message payload.
  /// One or both of the `data` and `notification` keys are required.
  factory MessagingPayload(
    Map<String, dynamic> data,
    messaging_interop.NotificationMessagePayload notification,
  ) =>
      MessagingPayload._fromJsObject(messaging_interop.MessagingPayloadJsImpl(
        data: jsify(data),
        notification: notification,
      ));

  MessagingPayload._fromJsObject(
      messaging_interop.MessagingPayloadJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  Map<String, dynamic> get data => dartify(jsObject.data);

  @override
  set data(Map<String, dynamic> v) => jsObject.data = jsify(v);

  @override
  messaging_interop.NotificationMessagePayload get notification =>
      jsObject.notification;

  @override
  set notification(messaging_interop.NotificationMessagePayload v) =>
      jsObject.notification = v;
}

class MessagingTopicManagementResponse extends JsObjectWrapper<
    messaging_interop.MessagingTopicManagementResponseJsImpl> {
  static final _expando = Expando<MessagingTopicManagementResponse>();

  MessagingTopicManagementResponse._fromJsObject(
      messaging_interop.MessagingTopicManagementResponseJsImpl jsObject)
      : super.fromJsObject(jsObject);

  List<admin_interop.FirebaseArrayIndexError> get errors =>
      List.from(jsObject.errors);

  num get failureCount => jsObject.failureCount;

  num get successCount => jsObject.successCount;

  static MessagingTopicManagementResponse getInstance(
      messaging_interop.MessagingTopicManagementResponseJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??=
        MessagingTopicManagementResponse._fromJsObject(jsObject);
  }
}

class MulticastMessage
    extends JsObjectWrapper<messaging_interop.MulticastMessageJsImpl>
    implements _MulticastMessage {
  factory MulticastMessage({
    AndroidConfig android,
    ApnsConfig apns,
    List<String> tokens,
    Map<String, dynamic> data,
    messaging_interop.FcmOptions fcmOptions,
    messaging_interop.MessagingNotification notification,
    WebpushConfig webpush,
  }) =>
      MulticastMessage._fromJsObject(messaging_interop.MulticastMessageJsImpl(
        android: android.jsObject,
        apns: apns.jsObject,
        tokens: jsify(tokens),
        data: jsify(data),
        fcmOptions: fcmOptions,
        notification: notification,
        webpush: webpush.jsObject,
      ));

  MulticastMessage._fromJsObject(
      messaging_interop.MulticastMessageJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  AndroidConfig get android => AndroidConfig.getInstance(jsObject.android);

  @override
  set android(AndroidConfig v) => jsObject.android = v.jsObject;

  @override
  ApnsConfig get apns => ApnsConfig.getInstance(jsObject.apns);

  @override
  set apns(ApnsConfig v) => jsObject.apns = v.jsObject;

  @override
  Map<String, dynamic> get data => dartify(jsObject.data);

  @override
  set data(Map<String, dynamic> v) => jsObject.data = jsify(v);

  @override
  messaging_interop.FcmOptions get fcmOptions => jsObject.fcmOptions;

  @override
  set fcmOptions(messaging_interop.FcmOptions v) => jsObject.fcmOptions = v;

  @override
  messaging_interop.MessagingNotification get notification =>
      jsObject.notification;

  @override
  set notification(messaging_interop.MessagingNotification v) =>
      jsObject.notification = v;

  @override
  List<String> get tokens => List.from(jsObject.tokens);

  @override
  set tokens(List<String> v) => jsObject.tokens = jsify(v);

  @override
  WebpushConfig get webpush => WebpushConfig.getInstance(jsObject.webpush);

  @override
  set webpush(WebpushConfig v) => jsObject.webpush = v.jsObject;
}

class TokenMessage extends JsObjectWrapper<messaging_interop.TokenMessageJsImpl>
    implements _TokenMessage {
  factory TokenMessage({
    AndroidConfig android,
    ApnsConfig apns,
    String token,
    Map<String, dynamic> data,
    messaging_interop.FcmOptions fcmOptions,
    messaging_interop.MessagingNotification notification,
    WebpushConfig webpush,
  }) =>
      TokenMessage._fromJsObject(messaging_interop.TokenMessageJsImpl(
        android: android.jsObject,
        apns: apns.jsObject,
        token: token,
        data: jsify(data),
        fcmOptions: fcmOptions,
        notification: notification,
        webpush: webpush.jsObject,
      ));

  TokenMessage._fromJsObject(messaging_interop.TokenMessageJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  AndroidConfig get android => AndroidConfig.getInstance(jsObject.android);

  @override
  set android(AndroidConfig v) => jsObject.android = v.jsObject;

  @override
  ApnsConfig get apns => ApnsConfig.getInstance(jsObject.apns);

  @override
  set apns(ApnsConfig v) => jsObject.apns = v.jsObject;

  @override
  Map<String, dynamic> get data => dartify(jsObject.data);

  @override
  set data(Map<String, dynamic> v) => jsObject.data = jsify(v);

  @override
  messaging_interop.FcmOptions get fcmOptions => jsObject.fcmOptions;

  @override
  set fcmOptions(messaging_interop.FcmOptions v) => jsObject.fcmOptions = v;

  @override
  messaging_interop.MessagingNotification get notification =>
      jsObject.notification;

  @override
  set notification(messaging_interop.MessagingNotification v) =>
      jsObject.notification = v;

  @override
  String get token => jsObject.token;

  @override
  set token(String v) => jsObject.token = v;

  @override
  WebpushConfig get webpush => WebpushConfig.getInstance(jsObject.webpush);

  @override
  set webpush(WebpushConfig v) => jsObject.webpush = v.jsObject;
}

class TopicMessage extends JsObjectWrapper<messaging_interop.TopicMessageJsImpl>
    implements _TopicMessage {
  factory TopicMessage({
    AndroidConfig android,
    ApnsConfig apns,
    String topic,
    Map<String, dynamic> data,
    messaging_interop.FcmOptions fcmOptions,
    messaging_interop.MessagingNotification notification,
    WebpushConfig webpush,
  }) =>
      TopicMessage._fromJsObject(messaging_interop.TopicMessageJsImpl(
        android: android.jsObject,
        apns: apns.jsObject,
        topic: topic,
        data: jsify(data),
        fcmOptions: fcmOptions,
        notification: notification,
        webpush: webpush.jsObject,
      ));

  TopicMessage._fromJsObject(messaging_interop.TopicMessageJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  AndroidConfig get android => AndroidConfig.getInstance(jsObject.android);

  @override
  set android(AndroidConfig v) => jsObject.android = v.jsObject;

  @override
  ApnsConfig get apns => ApnsConfig.getInstance(jsObject.apns);

  @override
  set apns(ApnsConfig v) => jsObject.apns = v.jsObject;

  @override
  Map<String, dynamic> get data => dartify(jsObject.data);

  @override
  set data(Map<String, dynamic> v) => jsObject.data = jsify(v);

  @override
  messaging_interop.FcmOptions get fcmOptions => jsObject.fcmOptions;

  @override
  set fcmOptions(messaging_interop.FcmOptions v) => jsObject.fcmOptions = v;

  @override
  messaging_interop.MessagingNotification get notification =>
      jsObject.notification;

  @override
  set notification(messaging_interop.MessagingNotification v) =>
      jsObject.notification = v;

  @override
  String get topic => jsObject.topic;

  @override
  set topic(String v) => jsObject.topic = v;

  @override
  WebpushConfig get webpush => WebpushConfig.getInstance(jsObject.webpush);

  @override
  set webpush(WebpushConfig v) => jsObject.webpush = v.jsObject;
}

class WebpushConfig
    extends JsObjectWrapper<messaging_interop.WebpushConfigJsImpl> {
  static final _expando = Expando<WebpushConfig>();

  WebpushConfig._fromJsObject(messaging_interop.WebpushConfigJsImpl jsObject)
      : super.fromJsObject(jsObject);

  Map<String, String> get data {
    if (jsObject.data == null) return null;
    final result = dartify(jsObject.data);
    return (result as Map<String, dynamic>).cast<String, String>();
  }

  messaging_interop.WebpushFcmOptions get fcmOptions => jsObject.fcmOptions;

  Map<String, String> get headers {
    if (jsObject.headers == null) return null;
    final result = dartify(jsObject.headers);
    return (result as Map<String, dynamic>).cast<String, String>();
  }

  WebpushNotification get notification =>
      WebpushNotification.getInstance(jsObject.notification);

  static WebpushConfig getInstance(
      messaging_interop.WebpushConfigJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= WebpushConfig._fromJsObject(jsObject);
  }
}

class WebpushNotification
    extends JsObjectWrapper<messaging_interop.WebpushNotificationJsImpl> {
  static final _expando = Expando<WebpushNotification>();

  WebpushNotification._fromJsObject(
      messaging_interop.WebpushNotificationJsImpl jsObject)
      : super.fromJsObject(jsObject);

  List<messaging_interop.WebPushActions> get actions =>
      List.from(jsObject.actions);

  String get badge => jsObject.badge;

  String get body => jsObject.body;

  Map<String, dynamic> get data => dartify(jsObject.data);

  String get dir => jsObject.dir;

  String get icon => jsObject.icon;

  String get image => jsObject.image;

  String get lang => jsObject.lang;

  bool get renotify => jsObject.renotify;

  bool get requireInteraction => jsObject.requireInteraction;

  bool get silent => jsObject.silent;

  String get tag => jsObject.tag;

  num get timestamp => jsObject.timestamp;

  String get title => jsObject.title;

  dynamic /*num | List<num>*/ get vibrate => jsObject.vibrate;

  static WebpushNotification getInstance(
      messaging_interop.WebpushNotificationJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= WebpushNotification._fromJsObject(jsObject);
  }
}

abstract class _AndroidConfig {
  /// Collapse key for the message. Collapse key serves as an identifier for a
  /// group of messages that can be collapsed, so that only the last message
  /// gets sent when delivery can be resumed. A maximum of four different
  /// collapse keys may be active at any given time.
  String collapseKey;

  /// A collection of data fields to be included in the message. All values
  /// must be strings.
  Map<String, String> data;

  /// Options for features provided by the FCM SDK for Android.
  messaging_interop.AndroidFcmOptions fcmOptions;

  /// Android notification to be included in the message.
  AndroidNotification notification;

  /// Priority of the message. Must be either normal or high.
  String priority;

  /// Package name of the application where the registration tokens must match
  /// in order to receive the message.
  String restrictedPackageName;

  /// Time-to-live duration of the message in milliseconds.
  num ttl;
}

abstract class _AndroidNotification {
  /// Body of the Android notification.
  String body;

  /// An array of resource keys that will be used in place of the format
  /// specifiers in bodyLocKey.
  String bodyLocArgs;

  /// Key of the body string in the app's string resource to use to localize
  /// the body text.
  String bodyLocKey;

  /// The Android notification channel ID (new in Android O). The app must
  /// create a channel with this channel ID before any notification with this
  /// channel ID can be received. If you don't send this channel ID in the
  /// request, or if the channel ID provided has not yet been created by the
  /// app, FCM uses the channel ID specified in the app manifest.
  String channelId;

  /// Action associated with a user click on the notification. If specified, an
  /// activity with a matching Intent Filter is launched when a user clicks on
  /// the notification.
  String clickAction;

  /// Notification icon color in #rrggbb format.
  String color;

  /// If set to true, use the Android framework's default LED light settings
  /// for the notification. Default values are specified in config.xml. If
  /// default_light_settings is set to true and light_settings is also set, the
  /// user-specified light_settings is used instead of the default value.
  bool defaultLightSettings;

  /// If set to true, use the Android framework's default sound for the
  /// notification. Default values are specified in config.xml.
  bool defaultSound;

  /// If set to true, use the Android framework's default vibrate pattern for
  /// the notification. Default values are specified in config.xml. If
  /// default_vibrate_timings is set to true and vibrate_timings is also set,
  /// the default value is used instead of the user-specified vibrate_timings.
  bool defaultVibrateTimings;

  /// For notifications that inform users about events with an absolute time
  /// reference, sets the time that the event in the notification occurred.
  /// Notifications in the panel are sorted by this time.
  DateTime eventTimestamp;

  /// Icon resource for the Android notification.
  String icon;

  /// URL of an image to be displayed in the notification.
  String imageUrl;

  /// Settings to control the notification's LED blinking rate and color if LED
  /// is available on the device. The total blinking time is controlled by the
  /// OS.
  messaging_interop.LightSettings lightSettings;

  /// Sets whether or not this notification is relevant only to the current
  /// device. Some notifications can be bridged to other devices for remote
  /// display, such as a Wear OS watch. This hint can be set to recommend this
  /// notification not be bridged. See Wear OS guides
  bool localOnly;

  /// Sets the number of items this notification represents. May be displayed
  /// as a badge count for Launchers that support badging. See
  /// [NotificationBadge(https:///developer.android.com/training/notify-user/
  /// badges). For example, this might be useful if you're using just one
  /// notification to represent multiple new messages but you want the count
  /// here to represent the number of total new messages. If zero or
  /// unspecified, systems that support badging use the default, which is to \
  /// increment a number displayed on the long-press menu each time a new
  /// notification arrives.
  num notificationCount;

  /// undefined | "high" | "min" | "low" | "default" | "max".
  /// Sets the relative priority for this notification. Low-priority
  /// notifications may be hidden from the user in certain situations. Note
  /// this priority differs from AndroidMessagePriority. This priority is
  /// processed by the client after the message has been delivered. Whereas
  /// AndroidMessagePriority is an FCM concept that controls when the message
  /// is delivered.
  String priority;

  /// File name of the sound to be played when the device receives the
  /// notification.
  String sound;

  /// When set to false or unset, the notification is automatically dismissed
  /// when the user clicks it in the panel. When set to true, the notification
  /// persists even when the user clicks it.
  bool sticky;

  /// Notification tag. This is an identifier used to replace existing
  /// notifications in the notification drawer. If not specified, each request
  /// creates a new notification.
  String tag;

  /// Sets the "ticker" text, which is sent to accessibility services. Prior to
  /// API level 21 (Lollipop), sets the text that is displayed in the status
  /// bar when the notification first arrives.
  String ticker;

  /// Title of the Android notification. When provided, overrides the title set
  /// via admin.messaging.Notification.
  String title;

  /// An array of resource keys that will be used in place of the format
  /// specifiers in titleLocKey.
  String titleLocArgs;

  /// Key of the title string in the app's string resource to use to localize
  /// the title text.
  String titleLocKey;

  /// `vibrateTimingsMillis`
  /// Sets the vibration pattern to use. Pass in an array of milliseconds to
  /// turn the vibrator on or off. The first value indicates the duration to
  /// wait before turning the vibrator on. The next value indicates the
  /// duration to keep the vibrator on. Subsequent values alternate between
  /// duration to turn the vibrator off and to turn the vibrator on. If
  /// vibrate_timings is set and default_vibrate_timings is set to true, the
  /// default value is used instead of the user-specified vibrate_timings.
  List<num> vibrateTimingsMillis;

  /// visibility: "private" | "public" | "secret"
  /// Sets the visibility of the notification. Must be either private, public,
  /// or secret. If unspecified, defaults to private.
  String visibility;
}

abstract class _ApnsConfig {
  messaging_interop.ApnsFcmOptions fcmOptions;

  Map<String, String> headers;

  ApnsPayload payload;
}

abstract class _BaseMessagee {
  AndroidConfig android;

  ApnsConfig apns;

  Map<String, dynamic> data;

  messaging_interop.FcmOptions fcmOptions;

  messaging_interop.MessagingNotification notification;

  WebpushConfig webpush;
}

abstract class _ConditionMessage extends _BaseMessagee {
  String condition;
}

abstract class _Messaging {
  /// The [App] associated with the current [Messaging] service instance.
  App get app;

  /// Sends the given message via FCM.
  ///
  /// `message`:
  /// The message payload. Must be either [TokenMessage], [TopicMessage], or
  /// [ConditionMessage]
  ///
  /// `dryRun`:
  /// Whether to send the message in the dry-run (validation only) mode.
  Future<String> send(
    dynamic /*TokenMessage | TopicMessage | ConditionMessage*/ message, {
    bool dryRun,
  });

  /// Sends all the messages in the given array via Firebase Cloud Messaging.
  /// Employs batching to send the entire list as a single RPC call. Compared
  /// to the [send()] method, this method is a significantly more efficient way
  /// to send multiple messages.
  /// The responses list obtained from the return value corresponds to the
  /// order of tokens in the [MulticastMessage]. An error from this method
  /// indicates a total failure -- i.e. none of the messages in the list could
  /// be sent. Partial failures are indicated by a [BatchResponse] return value.
  ///
  /// `messages`:
  /// A non-empty array containing up to 500 messages. Must be either
  /// [TokenMessage], [TopicMessage], or [ConditionMessage]
  ///
  /// `dryRun`:
  /// Whether to send the message in the dry-run (validation only) mode.
  Future<BatchResponse> sendAll(
    List /*TokenMessage | TopicMessage | ConditionMessage*/ messages, {
    bool dryRun,
  });

  /// Sends the given multicast message to all the FCM registration tokens
  /// specified in it.
  ///
  /// This method uses the [sendAll()] API under the hood to send the given
  /// message to all the target recipients. The responses list obtained from
  /// the return value corresponds to the order of tokens in the
  /// [MulticastMessage]. An error from this method indicates a total failure --
  /// i.e. the message was not sent to any of the tokens in the list. Partial
  /// failures are indicated by a BatchResponse return value.
  ///
  /// `message`:
  /// A multicast message containing up to 500 tokens.
  /// `dryRun`:
  /// Whether to send the message in the dry-run (validation only) mode.
  Future<BatchResponse> sendMulticast(
    MulticastMessage message, {
    bool dryRun,
  });

  /// Sends an FCM message to a condition.
  ///
  /// `condition`: The condition determining to which topics to send the
  /// message.
  ///
  /// `payload`: The message payload.
  ///
  /// `options`: Optional options to alter the message.
  Future<messaging_interop.MessagingConditionResponse> sendToCondition(
    String condition,
    MessagingPayload payload, [
    messaging_interop.MessagingOptions options,
  ]);

  /// Sends an FCM message to a single device corresponding to the provided
  /// registration token.
  ///
  /// `registrationToken`:
  /// A device registration token or an array of device registration tokens to
  /// which the message should be sent.
  ///
  /// `payload`: The message payload.
  ///
  /// `options`: Optional options to alter the message.
  Future<messaging_interop.MessagingDevicesResponse> sendToDevice(
    List<String> registrationToken,
    MessagingPayload payload, [
    messaging_interop.MessagingOptions options,
  ]);

  /// Sends an FCM message to a device group corresponding to the provided
  /// notification key.
  ///
  /// `notificationKey`:
  /// The notification key for the device group to which to send the message.
  ///
  /// `payload`: The message payload.
  ///
  /// `options`: Optional options to alter the message.
  Future<MessagingDeviceGroupResponse> sendToDeviceGroup(
    String notificationKey,
    MessagingPayload payload, [
    messaging_interop.MessagingOptions options,
  ]);

  /// Sends an FCM message to a topic.
  ///
  /// `topic`:
  /// The topic to which to send the message.
  ///
  /// `payload`: The message payload.
  ///
  /// `options`: Optional options to alter the message.
  Future<messaging_interop.MessagingTopicResponse> sendToTopic(
    String topic,
    MessagingPayload payload, [
    messaging_interop.MessagingOptions options,
  ]);

  /// Subscribes a device to an FCM topic.
  ///
  /// `registrationToken`: A token or array of registration tokens for the
  /// devices to subscribe to the topic.
  ///
  /// `topic:` The topic to which to subscribe.
  Future<MessagingTopicManagementResponse> subscribeToTopic(
    List<String> registrationToken,
    String topic,
  );

  /// Unsubscribes a device from an FCM topic.
  ///
  /// `registrationTokens`: A token or array of registration tokens for the
  /// devices to subscribe to the topic.
  ///
  /// `topic:` The topic to which to subscribe.
  Future<MessagingTopicManagementResponse> unsubscribeFromTopic(
    List<String> registrationToken,
    String topic,
  );
}

abstract class _MessagingPayload {
  /// The data message payload.
  Map<String, dynamic> data;

  /// The notification message payload.
  messaging_interop.NotificationMessagePayload notification;
}

abstract class _MulticastMessage extends _BaseMessagee {
  List<String> tokens;
}

abstract class _TokenMessage extends _BaseMessagee {
  String token;
}

abstract class _TopicMessage extends _BaseMessagee {
  String topic;
}

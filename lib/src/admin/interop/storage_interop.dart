// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.interop.storage;

import 'package:js/js.dart';
import 'package:node_interop/stream.dart';

import '../../utils/es6_interop.dart';
import 'app_interop.dart';

@JS()
@anonymous
abstract class Acl {}

@JS()
@anonymous
abstract class AddLifecycleRuleOptions {
  external factory AddLifecycleRuleOptions({bool append});
  external bool get append;
  external set append(bool v);
}

@JS()
@anonymous
abstract class BucketExistsOptions {
  external factory BucketExistsOptions({
    String userProject,
    bool autoCreate,
  });
  external bool get autoCreate;
  external set autoCreate(bool v);
  external String get userProject;
  external set userProject(String v);
}

@JS()
@anonymous
abstract class BucketJsImpl {
  // external Acl get acl;
  // external Function get getFileStream;
  // external Iam get iam;
  // external String get name;
  // external UrlSigner get signer;
  // external StorageJsImpl get storage;
  // external String get userProject;
  external PromiseJsImpl<List> addLifecycleRule(
    LifeCycleRuleJsImpl rule, [
    AddLifecycleRuleOptions options,
    dynamic /*Function(JsError, dynamic)*/ callback,
  ]);
  external PromiseJsImpl<List> combine(
    dynamic /*List<String>|List<File>*/ sources,
    dynamic /*String | File*/ destination, [
    CombineOptions options,
    // dynamic /*Function(JsError,File,Map<String,dynamic>)*/ callback,
  ]);
  external PromiseJsImpl<List> create([
    CreateBucketRequestJsImpl metadata,
    // dynamic /*Function(JsError, Bucket, Map<String,dynamic>)*/ callback,
  ]);
  external PromiseJsImpl<List> createChannel(
    String id,
    CreateChannelConfig config, [
    StorageBasicOptions options,
    // dynamic /*Function(JsError,Channel,Map<String,dynamic>)*/ callback,
  ]);
  external PromiseJsImpl<List> createNotification(
    String topic, [
    CreateNotificationOptionsJsImpl options,
    // dynamic /*Function(JsError,Channel,Map<String,dynamic>)*/ callback,
  ]);
  external PromiseJsImpl<List> delete([
    StorageBasicOptions options,
    // dynamic /*Function(JsError,Map<String,dynamic>)*/ callback,
  ]);
  external PromiseJsImpl deleteFiles([
    DeleteFilesOptions options,
    // dynamic /*Function(dynamic,Map<String,dynamic>)*/ callback,
  ]);
  external PromiseJsImpl<List> deleteLabels(
      dynamic /*String | List<String>*/ labels);
  external PromiseJsImpl<List> disableRequesterPays();
  external PromiseJsImpl<List> enableLogging(
    EnableLoggingOptionsJsImpl options,
  );
  external PromiseJsImpl<List> enableRequesterPays();
  external PromiseJsImpl<List> exists([
    BucketExistsOptions options,
  ]);
  external FileJsImpl file(String name, [FileOptions options]);
  external PromiseJsImpl<List> get([
    GetBucketOptions options,
    // dynamic /*Function(JsError,Bucket,Map<String,dynamic>)*/ callback,
  ]);
  external PromiseJsImpl<List> getFiles([
    GetFilesOptions options,
    // dynamic /*Function(JsError,List<File>)*/ callback,
  ]);
  external PromiseJsImpl<List> getLabels([
    StorageBasicOptions options,
    // dynamic /*Function(JsError,Map<String,dynamic>)*/ callback,
  ]);
  external PromiseJsImpl<List> getMetadata([StorageBasicOptions options]);
  external PromiseJsImpl<List> getNotifications([
    StorageBasicOptions options,
  ]);
  external PromiseJsImpl<List> getSignedUrl(
      GetBucketSignedUrlConfigJsImpl options);
  external PromiseJsImpl<List> lock([dynamic /*String|num*/ metageneration]);
  external PromiseJsImpl<List> makePrivate([MakeBucketPrivateOptions options]);
  external PromiseJsImpl<List> makePublic([
    MakeBucketPublicOptions options,
  ]);
  external NotificationJsImpl notification(String id);
  external PromiseJsImpl<List> removeRetentionPeriod();
  external PromiseJsImpl<List> setCorsConfiguration(
    dynamic /*List<Cors>*/ cors,
  );
  external PromiseJsImpl<List> setLabels(
    dynamic /*Map<String,String>*/ labels, [
    StorageBasicOptions options,
    // dynamic /*Function(JsError,Map<String,dynamic>)*/ callback,
  ]);
  external PromiseJsImpl<List> setMetadata(
    dynamic /*Map<String,dynamic>*/ metadata, [
    StorageBasicOptions options,
    // dynamic /*Function(JsError,Map<String,dynamic>)*/ callback,
  ]);
  external PromiseJsImpl<List> setRetentionPeriod(num duration);
  external PromiseJsImpl<List> setStorageClass(
    String storageClass, [
    StorageBasicOptions options,
    // dynamic /*Function(JsError,Map<String,dynamic>)*/ callback,
  ]);
  external void setUserProject(String userProject);
  external PromiseJsImpl<List> upload(
    String pathString, [
    UploadOptionsJsImpl options,
    // dynamic /*Function(JsError,File,Map<String,dynamic>)*/ callback,
  ]);
}

@JS()
@anonymous
abstract class ChannelJsImpl {
  // external factory ChannelJsImpl(String id, String resourceId);
  external PromiseJsImpl stop();
}

@JS()
@anonymous
abstract class CombineOptions {
  external factory CombineOptions({
    String kmsKeyName,
    String userProject,
  });
  external String get kmsKeyName;
  external set kmsKeyName(String v);
  external String get userProject;
  external set userProject(String v);
}

@JS()
@anonymous
abstract class ContentLenghtRange {
  external factory ContentLenghtRange({
    num max,
    num min,
  });
  external num get max;
  external set max(num v);
  external num get min;
  external set min(num v);
}

@JS()
@anonymous
abstract class CopyOptions {
  external factory CopyOptions({
    String destinationKmsKeyName,
    String predefinedAcl,
    String token,
    String userProject,
  });
  external String get destinationKmsKeyName;
  external set destinationKmsKeyName(String v);
  external String get predefinedAcl;
  external set predefinedAcl(String v);
  external String get token;
  external set token(String v);
  external String get userProject;
  external set userProject(String v);
}

@JS()
@anonymous
abstract class CorsJsImpl {
  external factory CorsJsImpl({
    num maxAgeSeconds,
    List method,
    List origin,
    List responseHeader,
  });
  external num get maxAgeSeconds;
  external set maxAgeSeconds(num v);
  external List get method;
  external set method(List v);
  external List get origin;
  external set origin(List v);
  external List get responseHeader;
  external set responseHeader(List v);
}

@JS()
@anonymous
abstract class CreateBucketRequestJsImpl {
  external factory CreateBucketRequestJsImpl({
    bool archive,
    bool coldline,
    List /*Cors*/ cors,
    bool dra,
    bool multiRegional,
    bool nearline,
    bool regional,
    bool requesterPays,
    bool standard,
    bool userProject,
    Versioning versioning,
  });
  external bool get archive;
  external set archive(bool v);
  external bool get coldline;
  external set coldline(bool v);
  external List /*Cors*/ get cors;
  external set cors(List /*Cors*/ v);
  external bool get dra;
  external set dra(bool v);
  external bool get multiRegional;
  external set multiRegional(bool v);
  external bool get nearline;
  external set nearline(bool v);
  external bool get regional;
  external set regional(bool v);
  external bool get requesterPays;
  external set requesterPays(bool v);
  external bool get standard;
  external set standard(bool v);
  external bool get userProject;
  external set userProject(bool v);
  external Versioning get versioning;
  external set versioning(Versioning v);
}

@JS()
@anonymous
abstract class CreateChannelConfig {
  external factory CreateChannelConfig({
    String delimiter,
    num maxResults,
    String pageToken,
    String prefix,
    String projection,
    String userProject,
    bool versions,
    String address,
  });
  external String get address;
  external set address(String v);
  external String get delimiter;
  external set delimiter(String v);
  external num get maxResults;
  external set maxResults(num v);
  external String get pageToken;
  external set pageToken(String v);
  external String get prefix;
  external set prefix(String v);
  external String get projection;
  external set projection(String v);
  external String get userProject;
  external set userProject(String v);
  external bool get versions;
  external set versions(bool v);
}

@JS()
@anonymous
abstract class CreateNotificationOptionsJsImpl {
  external factory CreateNotificationOptionsJsImpl({
    dynamic /*Map<String,String>*/ customAttributes,
    String eventTypes,
    String objectNamePrefix,
    String payloadFormat,
    String userProject,
  });
  external dynamic /*Map<String,String>*/ get customAttributes;
  external set customAttributes(dynamic /*Map<String,String>*/ v);
  external String get eventTypes;
  external set eventTypes(String v);
  external String get objectNamePrefix;
  external set objectNamePrefix(String v);
  external String get payloadFormat;
  external set payloadFormat(String v);
  external String get userProject;
  external set userProject(String v);
}

@JS()
@anonymous
abstract class CreateReadStreamOptions {
  external factory CreateReadStreamOptions({
    String userProject,
    dynamic validation,
    num start,
    num end,
    bool decompress,
  });
  external bool get decompress;
  external set decompress(bool v);
  external num get end;
  external set end(num v);
  external num get start;
  external set start(num v);
  external String get userProject;
  external set userProject(String v);
  external dynamic get validation;
  external set validation(dynamic v);
}

@JS()
@anonymous
abstract class CreateResumableUploadOptionsJsImpl {
  external factory CreateResumableUploadOptionsJsImpl({
    String configPath,
    dynamic /*Map<String,dynamic>*/ metadata,
    String origin,
    num offset,
    String predefinedAcl,
    bool private,
    bool public,
    String uri,
    String userProject,
  });

  external String get configPath;
  external set configPath(String v);
  external dynamic /*Map<String,dynamic>*/ get metadata;
  external set metadata(dynamic /*Map<String,dynamic>*/ v);
  external num get offset;
  external set offset(num v);
  external String get origin;
  external set origin(String v);
  external String
/*'authenticatedRead' | 'bucketOwnerFullControl' | 'bucketOwnerRead' | 
'private' | 'projectPrivate' | 'publicRead'*/
      get predefinedAcl;
  external set predefinedAcl(String v);
  external bool get private;
  external set private(bool v);
  external bool get public;
  external set public(bool v);
  external String get uri;
  external set uri(String v);
  external String get userProject;
  external set userProject(String v);
}

@JS()
@anonymous
abstract class CreateWriteStreamOptionsJsImpl {
  external factory CreateWriteStreamOptionsJsImpl({
    String configPath,
    String contentType,
    dynamic /*String | bool*/ gzip,
    dynamic /*Map<String,dynamic>*/ metadata,
    num offset,
    String predefinedAcl,
    bool private,
    bool public,
    bool resumable,
    String uri,
    String userProject,
    dynamic /*String | bool*/ validation,
  });

  external String get configPath;
  external set configPath(String v);
  external String get contentType;
  external set contentType(String v);
  external dynamic /*String|bool*/ get gzip;
  external set gzip(dynamic /*String|bool*/ v);
  external dynamic /*Map<String,dynamic>*/ get metadata;
  external set metadata(dynamic /*Map<String,dynamic>*/ v);
  external num get offset;
  external set offset(num v);
  external String get predefinedAcl;
  external set predefinedAcl(String v);
  external bool get private;
  external set private(bool v);
  external bool get public;
  external set public(bool v);
  external bool get resumable;
  external set resumable(bool v);
  external String get uri;
  external set uri(String v);
  external String get userProject;
  external set userProject(String v);
  external dynamic /*String | bool*/ get validation;
  external set validation(dynamic /*String | bool*/ v);
}

@JS()
@anonymous
abstract class DeleteFilesOptions implements GetFilesOptions {
  external factory DeleteFilesOptions({
    bool autoPaginate,
    String delimiter,
    String directory,
    String prefix,
    num maxApiCalls,
    num maxResults,
    String paoken,
    String userProject,
    bool versions,
    bool force,
  });
  external bool get force;
  external set force(bool v);
}

@JS()
@anonymous
abstract class EnableLoggingOptionsJsImpl {
  external factory EnableLoggingOptionsJsImpl({
    dynamic bucket,
    String prefix,
  });
  external dynamic /*String | Bucket*/ get bucket;
  external set bucket(dynamic /*String | Bucket*/ v);
  external String get prefix;
  external set prefix(String v);
}

@JS()
@anonymous
abstract class EncryptionKeyOptions {
  external factory EncryptionKeyOptions({
    String encryptionKey,
    String kmsKeyName,
  });
  external String get encryptionKey;
  external set encryptionKey(String v);
  external String get kmsKeyName;
  external set kmsKeyName(String v);
}

@JS()
@anonymous
abstract class FileJsImpl {
  //TODO: generateSignedPostPolicyV4, getSignedUrl, save
  external PromiseJsImpl<List> copy(
    dynamic /*String|Bucket|File*/ destination, [
    CopyOptions options,
  ]);
  external PromiseJsImpl<Readable> createReadStream(
      [CreateReadStreamOptions options]);
  external PromiseJsImpl<List> createResumableUpload(
      [CreateResumableUploadOptionsJsImpl options]);
  external PromiseJsImpl<Writable> createWriteStream(
      [CreateWriteStreamOptionsJsImpl options]);
  external PromiseJsImpl<List> delete([StorageBasicOptions options]);
  external void deleteResumableCache();
  external PromiseJsImpl<List> download([CreateReadStreamOptions options]);
  external PromiseJsImpl<List> exists([StorageBasicOptions options]);
  external PromiseJsImpl<List> generateSignedPostPolicyV2(
      GetSignedPolicyOptionsJsImpl options);
  external PromiseJsImpl<List> get([StorageBasicOptions options]);
  external PromiseJsImpl<List> getExpirationDate();
  external PromiseJsImpl<List> getMetadata([StorageBasicOptions options]);
  external PromiseJsImpl<List> getSignedPolicy(
      GetSignedPolicyOptionsJsImpl options);
  external PromiseJsImpl<List> isPublic();
  external PromiseJsImpl<List> makePrivate([MakeFilePrivateOptions options]);
  external PromiseJsImpl<List> makePublic();
  external PromiseJsImpl<List> move(dynamic /*String|Bucket|File*/ destination);
  external PromiseJsImpl<List> rotateEncryptionKey(
      [EncryptionKeyOptions options]);
  external FileJsImpl setEncryptionKey(String encryptionKey);
  external PromiseJsImpl<List> setMetadata(
      [dynamic /*Map<String,dyamic>*/ metadata, StorageBasicOptions options]);
  external PromiseJsImpl<List> setStorageClass(String storageClass,
      [StorageBasicOptions options]);
  external void setUserProject(String userProject);
}

@JS()
@anonymous
abstract class FileOptions {
  external factory FileOptions({
    String encryptionKey,
    dynamic /*num | String*/ generation,
    String kmsKeyName,
    String userProject,
  });
  external String get encryptionKey;
  external set encryptionKey(String v);
  external dynamic get generation;
  external set generation(dynamic v);
  external String get kmsKeyName;
  external set kmsKeyName(String v);
  external String get userProject;
  external set userProject(String v);
}

@JS()
@anonymous
abstract class GetBucketOptions implements GetConfig {
  external factory GetBucketOptions({
    String userProject,
    bool autoCreate,
  });
  external String get userProject;
  external set userProject(String v);
}

@JS()
@anonymous
abstract class GetBucketSignedUrlConfigJsImpl {
  external factory GetBucketSignedUrlConfigJsImpl({
    String action,
    String cname,
    dynamic /*String|num|JsDate*/ expires,
    dynamic /*Map<String,dynamic>*/ extensionHeaders,
    dynamic /*Map<String,String>*/ queryParams,
    String version,
    bool virtualHostedStyle,
  });
  external String get action;
  external set action(String v);
  external String get cname;
  external set cname(String v);
  external dynamic /*String|num|JsDate*/ get expires;
  external /*String|num|JsDate*/ set expires(dynamic v);
  external dynamic /*Map<String,dynamic>*/ get extensionHeaders;
  external /*Map<String,dynamic>*/ set extensionHeaders(dynamic v);
  external dynamic /*Map<String,String>*/ get queryParams;
  external /*Map<String,String>*/ set queryParams(dynamic v);
  external String get version;
  external set version(String v);
  external bool get virtualHostedStyle;
  external set virtualHostedStyle(bool v);
}

@JS()
@anonymous
abstract class GetBucketsRequest {
  external factory GetBucketsRequest({
    bool autoPaginate,
    num maxApiCalls,
    num maxResults,
    String pageToken,
    String userProject,
  });
  external bool get autoPaginate;
  external set autoPaginate(bool v);
  external num get maxApiCalls;
  external set maxApiCalls(num v);
  external num get maxResults;
  external set maxResults(num v);
  external String get pageToken;
  external set pageToken(String v);
  external String get userProject;
  external set userProject(String v);
}

@JS()
@anonymous
abstract class GetConfig {
  external factory GetConfig({bool autoCreate});
  external bool get autoCreate;
  external set autoCreate(bool v);
}

@JS()
@anonymous
abstract class GetFilesOptions {
  external factory GetFilesOptions({
    bool autoPaginate,
    String delimiter,
    String directory,
    String prefix,
    num maxApiCalls,
    num maxResults,
    String pageToken,
    String userProject,
    bool versions,
  });
  external bool get autoPaginate;
  external set autoPaginate(bool v);
  external String get delimiter;
  external set delimiter(String v);
  external String get directory;
  external set directory(String v);
  external num get maxApiCalls;
  external set maxApiCalls(num v);
  external num get maxResults;
  external set maxResults(num v);
  external String get pageToken;
  external set pageToken(String v);
  external String get prefix;
  external set prefix(String v);
  external String get userProject;
  external set userProject(String v);
  external bool get versions;
  external set versions(bool v);
}

@JS()
@anonymous
abstract class GetSignedPolicyOptionsJsImpl {
  external factory GetSignedPolicyOptionsJsImpl({
    dynamic equals,
    dynamic expires,
    dynamic startsWith,
    String acl,
    String successRedirect,
    String successStatus,
    ContentLenghtRange contentLenghtRange,
  });
  external String get acl;
  external ContentLenghtRange get contentLenghtRange;
  external dynamic get equals;
  external dynamic get expires;
  external dynamic get startsWith;
  external String get successRedirect;
  external String get successStatus;
}

/// An HmacKey object contains metadata of an HMAC key created from a service
/// accou
/// @JS()nt through the Storage client using Storage#createHmacKey.
/// @anonymous
abstract class HmacKeyJsImpl {}

@JS()
@anonymous
abstract class Iam {}

@JS()
@anonymous
abstract class LifeCycleRuleAction {
  external factory LifeCycleRuleAction({String type, String storageClass});
  String get storageClass;
  set storageClass(String v);

  String get type;
  set type(String v);
}

@JS()
@anonymous
abstract class LifeCycleRuleJsImpl {
  external factory LifeCycleRuleJsImpl({
    LifeCycleRuleAction action,
    dynamic condition,
    String storageClass,
  });
  external LifeCycleRuleAction get action;
  external set action(LifeCycleRuleAction v);
  external dynamic /*Map<String,dynamic>*/ get condition;
  external set condition(dynamic /*Map<String,dynamic>*/ v);
  external String get storageClass;
  external set storageClass(String v);
}

@JS()
@anonymous
abstract class MakeAllFilesPublicPrivateOptions {
  external factory MakeAllFilesPublicPrivateOptions({
    bool force,
    bool private,
    bool public,
    String userProject,
  });
  external bool get force;
  external set force(bool v);
  external bool get private;
  external set private(bool v);
  external bool get public;
  external set public(bool v);
  external String get userProject;
  external set userProject(String v);
}

@JS()
@anonymous
abstract class MakeBucketPrivateOptions {
  external factory MakeBucketPrivateOptions({
    bool includeFiles,
    bool force,
    String userProject,
  });
  external bool get force;
  external set force(bool v);
  external bool get includeFiles;
  external set includeFiles(bool v);
  external String get userProject;
  external set userProject(String v);
}

@JS()
@anonymous
abstract class MakeBucketPublicOptions {
  external factory MakeBucketPublicOptions({
    bool includeFiles,
    bool force,
  });
  external bool get force;
  external set force(bool v);
  external bool get includeFiles;
  external set includeFiles(bool v);
}

@JS()
@anonymous
abstract class MakeFilePrivateOptions {
  external factory MakeFilePrivateOptions({
    bool strict,
    String userProject,
  });
  external bool get strict;
  external set strict(bool v);
  external String get userProject;
  external set userProject(String v);
}

@JS()
@anonymous
abstract class NotificationJsImpl {
  external PromiseJsImpl<List> delete([
    StorageBasicOptions options,
    // dynamic /*Function(JsError,Map<String,dynamic>)*/ callback,
  ]);

  external PromiseJsImpl<List> exist(
    String topic, [
    dynamic /*Map<String,dynamic>*/ options,
    // dynamic /*Function(JsError,bool)*/ callback,
  ]);

  external PromiseJsImpl<List> get([
    GetBucketOptions options,
    // dynamic /*Function(JsError,Map<String,dynamic>)*/ callback,
  ]);

  external PromiseJsImpl<List> getMetadata([StorageBasicOptions options]);
}

@JS()
@anonymous
abstract class PolicyDocument {
  external String get base64;
  external String get signature;
  external String get string;
}

@JS()
@anonymous
abstract class StorageBasicOptions {
  external factory StorageBasicOptions({String userProject});
  String get userProject;
  set userProject(String v);
}

@JS()
@anonymous
abstract class StorageBuilderJsImpl {
  external BucketJsImpl bucket();
}

@JS()
@anonymous
abstract class StorageJsImpl {
  external AppJsImpl get app;
  external BucketJsImpl bucket([String name]);
}

@JS()
@anonymous
abstract class UploadOptionsJsImpl
    implements CreateResumableUploadOptionsJsImpl {
  external factory UploadOptionsJsImpl({
    String configPath,
    dynamic /*Map<String,dynamic>*/ metadata,
    String origin,
    num offset,
    String predefinedAcl,
    bool private,
    bool public,
    String uri,
    String userProject,
    String destination,
    String encryptionKey,
    String kmsKeyName,
    bool resumable,
  });

  external dynamic /*String|File*/ get destination;
  external set destination(dynamic v);
  external String get encryptionKey;
  external set encryptionKey(String v);
  external String get kmsKeyName;
  external set kmsKeyName(String v);
  external bool get resumable;
  external set resumable(bool v);
}

@JS()
@anonymous
abstract class UrlSigner {}

@JS()
@anonymous
abstract class Versioning {
  external factory Versioning({bool enabled});
  external bool get enabled;
  external set enabled(bool v);
}

// ignore_for_file: public_member_api_docs
@JS('firebaseAdmin.credential')
library firebase_node.admin.interop.credential;

import 'package:js/js.dart';
import 'package:node_interop/http.dart';

import '../../utils/es6_interop.dart';
import 'admin_interop.dart';

@JS('Credential')
@anonymous
// ignore: one_member_abstracts
abstract class CredentialJsImpl {
  PromiseJsImpl<GoogleOAuthAccessToken> getAccessToken();
}

external CredentialJsImpl applicationDefault([HttpAgent httpAgent]);

external CredentialJsImpl cert(dynamic serviceAccountPathOrObject,
    [HttpAgent httpAgent]);

external CredentialJsImpl refreshToken(dynamic refreshTokenPathOrObject,
    [HttpAgent httpAgent]);

part of '../../admin.dart';

class SecurityRules extends JsObjectWrapper<sr_interop.SecurityRulesJsImpl>
    implements _SecurityRules {
  static final _expando = Expando<SecurityRules>();

  SecurityRules._fromJsObject(sr_interop.SecurityRulesJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  App get app => App.getInstance(jsObject.app);

  @override
  Future<sr_interop.Ruleset> createRuleset(sr_interop.RulesFile file) =>
      handleThenable(jsObject.createRuleset(file));

  @override
  sr_interop.RulesFile createRulesFileFromSource(String name, String source) =>
      jsObject.createRulesFileFromSource(name, source);

  @override
  Future<void> deleteRuleset(String name) =>
      handleThenable(jsObject.deleteRuleset(name));

  @override
  Future<sr_interop.Ruleset> getFirestoreRuleset() =>
      handleThenable(jsObject.getFirestoreRuleset());

  @override
  Future<sr_interop.Ruleset> getRuleset(String name) =>
      handleThenable(jsObject.getRuleset(name));

  @override
  Future<sr_interop.Ruleset> getStorageRuleset([String bucket]) {
    if (bucket == null) return handleThenable(jsObject.getStorageRuleset());
    return handleThenable(jsObject.getStorageRuleset(bucket));
  }

  @override
  Future<sr_interop.RulesetMetadataList> listRulesetMetadata(
          [num pageSize, String nextPageToken]) =>
      handleThenable(jsObject.listRulesetMetadata(pageSize, nextPageToken));

  @override
  Future<void> releaseFirestoreRuleset(dynamic ruleset) =>
      handleThenable(jsObject.releaseFirestoreRuleset(ruleset));

  @override
  Future<sr_interop.Ruleset> releaseFirestoreRulesetFromSource(String source) =>
      handleThenable(jsObject.releaseFirestoreRulesetFromSource(source));

  @override
  Future<void> releaseStorageRuleset(dynamic ruleset, [String bucket]) {
    if (bucket == null) {
      return handleThenable(jsObject.releaseStorageRuleset(ruleset));
    } else {
      return handleThenable(jsObject.releaseStorageRuleset(ruleset, bucket));
    }
  }

  @override
  Future<void> releaseStorageRulesetFromSource(dynamic ruleset,
      [String bucket]) {
    if (bucket == null) {
      return handleThenable(jsObject.releaseStorageRulesetFromSource(ruleset));
    } else {
      return handleThenable(
          jsObject.releaseStorageRulesetFromSource(ruleset, bucket));
    }
  }

  static SecurityRules getInstance(sr_interop.SecurityRulesJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= SecurityRules._fromJsObject(jsObject);
  }
}

abstract class _SecurityRules {
  App get app;
  Future<sr_interop.Ruleset> createRuleset(sr_interop.RulesFile file);
  sr_interop.RulesFile createRulesFileFromSource(String name, String source);
  Future<void> deleteRuleset(String name);
  Future<sr_interop.Ruleset> getFirestoreRuleset();
  Future<sr_interop.Ruleset> getRuleset(String name);
  Future<sr_interop.Ruleset> getStorageRuleset([String bucket]);
  Future<sr_interop.RulesetMetadataList> listRulesetMetadata(
      [num pageSize, String nextPageToken]);
  Future<void> releaseFirestoreRuleset(
      /*String | RulesetMetadata */ dynamic ruleset);
  Future<sr_interop.Ruleset> releaseFirestoreRulesetFromSource(String source);
  Future<void> releaseStorageRuleset(
    /*String | RulesetMetadata */ dynamic ruleset, [
    String bucket,
  ]);
  Future<void> releaseStorageRulesetFromSource(
    /*String | RulesetMetadata */ dynamic ruleset, [
    String bucket,
  ]);
}

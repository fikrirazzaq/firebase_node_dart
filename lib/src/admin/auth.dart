part of '../../admin.dart';

///Gets the Auth service for the default app or a given app.
///`admin.auth()` can be called with no arguments to access the default app's
///Auth service or as `admin.auth(app)` to access the Auth service associated
///with a specific app.
class Auth extends JsObjectWrapper<auth_interop.AuthJsImpl> implements _Auth {
  static final _expando = Expando<Auth>();

  Auth._fromJsObject(auth_interop.AuthJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  Future<String> createCustomToken(
    String uid, [
    Map<String, dynamic> developerClaims,
  ]) {
    if (developerClaims == null) {
      return handleThenable(jsObject.createCustomToken(uid));
    } else {
      return handleThenable(
          jsObject.createCustomToken(uid, jsify(developerClaims)));
    }
  }

  @override
  Future<auth_interop.AuthProviderConfig> createProviderConfig(
    auth_interop.AuthProviderConfig config,
  ) =>
      handleThenable(jsObject.createProviderConfig(config));

  @override
  Future<String> createSessionCookie(
    String idToken,
    auth_interop.SessionCookieOptions sessionCookieOptions,
  ) =>
      handleThenable(
          jsObject.createSessionCookie(idToken, sessionCookieOptions));

  @override
  Future<UserRecord> createUser(auth_interop.CreateRequest properties) =>
      handleThenable(jsObject.createUser(properties))
          .then(UserRecord.getInstance);

  @override
  Future<void> deleteProviderConfig(String providerId) =>
      handleThenable(jsObject.deleteProviderConfig(providerId));

  @override
  Future<void> deleteUser(String uid) =>
      handleThenable(jsObject.deleteUser(uid));

  @override
  Future<String> generateEmailVerificationLink(
    String email, [
    auth_interop.ActionCodeSettings actionCodeSettings,
  ]) {
    if (actionCodeSettings == null) {
      return handleThenable(jsObject.generateEmailVerificationLink(email));
    }
    return handleThenable(
        jsObject.generateEmailVerificationLink(email, actionCodeSettings));
  }

  @override
  Future<String> generatePasswordResetLink(
    String email, [
    auth_interop.ActionCodeSettings actionCodeSettings,
  ]) {
    if (actionCodeSettings == null) {
      return handleThenable(jsObject.generateEmailVerificationLink(email));
    }
    return handleThenable(
        jsObject.generateEmailVerificationLink(email, actionCodeSettings));
  }

  @override
  Future<String> generateSignInWithEmailLink(
    String email, [
    auth_interop.ActionCodeSettings actionCodeSettings,
  ]) {
    if (actionCodeSettings == null) {
      return handleThenable(jsObject.generateEmailVerificationLink(email));
    }
    return handleThenable(
        jsObject.generateEmailVerificationLink(email, actionCodeSettings));
  }

  @override
  Future<auth_interop.AuthProviderConfig> getProviderConfig(
          String providerId) =>
      handleThenable(jsObject.getProviderConfig(providerId));

  @override
  Future<UserRecord> getUser(String uid) =>
      handleThenable(jsObject.getUser(uid)).then(UserRecord.getInstance);

  @override
  Future<UserRecord> getUserByEmail(String uid) =>
      handleThenable(jsObject.getUserByEmail(uid)).then(UserRecord.getInstance);

  @override
  Future<UserRecord> getUserByPhoneNumber(String uid) =>
      handleThenable(jsObject.getUserByPhoneNumber(uid))
          .then(UserRecord.getInstance);

  @override
  Future<auth_interop.UsersResult> importUsers(
    List<auth_interop.UserImportRecord> users, [
    auth_interop.UserImportOptions options,
  ]) {
    if (options == null) return handleThenable(jsObject.importUsers(users));
    return handleThenable(jsObject.importUsers(users, options));
  }

  @override
  Future<auth_interop.ListProviderConfigResults> listProviderConfigs(
    auth_interop.AuthProviderConfigFilter options,
  ) =>
      handleThenable(jsObject.listProviderConfigs(options));

  @override
  Future<auth_interop.ListUsersResult> listUsers([
    int maxResults,
    String pageToken,
  ]) {
    if (pageToken != null) {
      return handleThenable(jsObject.listUsers(maxResults, pageToken));
    } else if (maxResults != null) {
      return handleThenable(jsObject.listUsers(maxResults));
    } else {
      return handleThenable(jsObject.listUsers());
    }
  }

  @override
  Future<void> revokeRefreshTokens(String uid) =>
      handleThenable(jsObject.revokeRefreshTokens(uid));

  @override
  Future<void> setCustomUserClaims(
    String uid,
    Map<String, dynamic> customUserClaims,
  ) =>
      handleThenable(
          jsObject.setCustomUserClaims(uid, jsify(customUserClaims)));

  @override
  TenantManager tenantManager() =>
      TenantManager.getInstance(jsObject.tenantManager());

  @override
  Future<auth_interop.AuthProviderConfig> updateProviderConfig(
    String providerId,
    /*OIDCUpdateAuthProviderRequest | SAMLUpdateAuthProviderRequest */
    dynamic updatedConfig,
  ) =>
      handleThenable(jsObject.updateProviderConfig(providerId, updatedConfig));

  @override
  Future<UserRecord> updateUser(
          String uid, auth_interop.UpdateRequest properties) =>
      handleThenable(jsObject.updateUser(uid, properties))
          .then(UserRecord.getInstance);

  @override
  Future<Map<String, dynamic>> verifyIdToken(
    String idToken, {
    bool checkRevoked = false,
  }) async {
    final result =
        await handleThenable(jsObject.verifyIdToken(idToken, checkRevoked))
            .then(dartify);
    return result;
  }

  @override
  Future<Map<String, dynamic>> verifySessionCookie(
    String sesionCookie, {
    bool checkForRevocation = false,
  }) async {
    final result = await handleThenable(
            jsObject.verifySessionCookie(sesionCookie, checkForRevocation))
        .then(dartify);
    return result;
  }

  ///Get instance of `Auth` from JS Object
  static Auth getInstance(auth_interop.AuthJsImpl jsObject) {
    if (jsObject == null) return null;

    return _expando[jsObject] = Auth._fromJsObject(jsObject);
  }
}

///Represents the result of the admin.auth.getUsers() API.
class GetUsersResult
    extends JsObjectWrapper<auth_interop.GetUsersResultJsImpl> {
  GetUsersResult._fromJsObject(auth_interop.GetUsersResultJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Set of identifiers that were requested, but not found.
  ///
  ///It is a list of User Identifier object with the keys is either uid, email,
  ///phoneNumber, providerId, or providerUid
  List<Map<String, dynamic>> get notFound => jsObject.notFound
      .map(dartify)
      .cast<Map>()
      .map((e) => e.cast<String, dynamic>())
      .toList();

  ///Set of user records, corresponding to the set of users that were
  ///requested. Only users that were found are listed here. The result set is
  ///unordered.
  List<UserRecord> get users =>
      (jsObject.users).map<UserRecord>(UserRecord.getInstance).toList();
}

///Tenant-aware Auth interface used for managing users, configuring SAML/OIDC
///providers, generating email links for password reset, email verification,
///etc for specific tenants.
///
/// Multi-tenancy support requires Google Cloud's Identity Platform (GCIP). To
/// learn more about GCIP, including pricing and features, see the GCIP
/// documentation
///
/// Each tenant contains its own identity providers, settings and sets of
/// users. Using TenantAwareAuth, users for a specific tenant and corresponding
/// OIDC/SAML configurations can also be managed, ID tokens for users signed in
/// to a specific tenant can be verified, and email action links can also be
/// generated for users belonging to the tenant.
///
/// TenantAwareAuth instances for a specific tenantId can be instantiated by
/// calling auth.tenantManager().authForTenant(tenantId).
class TenantAwareAuth
    extends JsObjectWrapper<auth_interop.TenantAwareAuthJsImpl>
    implements _TenantAwareAuth {
  static final _expando = Expando<TenantAwareAuth>();

  TenantAwareAuth._fromJsObject(auth_interop.TenantAwareAuthJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  String get tenantId => jsObject.tenantId;

  @override
  Future<String> createCustomToken(
    String uid, [
    Map<String, dynamic> developerClaims,
  ]) {
    if (developerClaims == null) {
      return handleThenable(jsObject.createCustomToken(uid));
    } else {
      return handleThenable(
          jsObject.createCustomToken(uid, jsify(developerClaims)));
    }
  }

  @override
  Future<auth_interop.AuthProviderConfig> createProviderConfig(
    auth_interop.AuthProviderConfig config,
  ) =>
      handleThenable(jsObject.createProviderConfig(config));

  @override
  Future<String> createSessionCookie(
    String idToken,
    auth_interop.SessionCookieOptions sessionCookieOptions,
  ) =>
      handleThenable(
          jsObject.createSessionCookie(idToken, sessionCookieOptions));

  @override
  Future<UserRecord> createUser(auth_interop.CreateRequest properties) =>
      handleThenable(jsObject.createUser(properties))
          .then(UserRecord.getInstance);

  @override
  Future<void> deleteProviderConfig(String providerId) =>
      handleThenable(jsObject.deleteProviderConfig(providerId));

  @override
  Future<void> deleteUser(String uid) =>
      handleThenable(jsObject.deleteUser(uid));

  @override
  Future<auth_interop.UsersResult> deleteUsers(List<String> uids) =>
      handleThenable(jsObject.deleteUsers(uids));

  @override
  Future<String> generateEmailVerificationLink(
    String email, [
    auth_interop.ActionCodeSettings actionCodeSettings,
  ]) {
    if (actionCodeSettings == null) {
      return handleThenable(jsObject.generateEmailVerificationLink(email));
    } else {
      return handleThenable(
          jsObject.generateEmailVerificationLink(email, actionCodeSettings));
    }
  }

  @override
  Future<String> generatePasswordResetLink(
    String email, [
    auth_interop.ActionCodeSettings actionCodeSettings,
  ]) {
    if (actionCodeSettings == null) {
      return handleThenable(jsObject.generatePasswordResetLink(email));
    } else {
      return handleThenable(
          jsObject.generatePasswordResetLink(email, actionCodeSettings));
    }
  }

  @override
  Future<String> generateSignInWithEmailLink(
    String email, [
    auth_interop.ActionCodeSettings actionCodeSettings,
  ]) {
    if (actionCodeSettings == null) {
      return handleThenable(jsObject.generateSignInWithEmailLink(email));
    } else {
      return handleThenable(
          jsObject.generateSignInWithEmailLink(email, actionCodeSettings));
    }
  }

  @override
  Future<auth_interop.AuthProviderConfig> getProviderConfig(
          String providerid) =>
      handleThenable(jsObject.getProviderConfig(providerid));

  @override
  Future<UserRecord> getUser(String uid) =>
      handleThenable(jsObject.getUser(uid)).then(UserRecord.getInstance);

  @override
  Future<UserRecord> getUserByEmail(String email) =>
      handleThenable(jsObject.getUserByEmail(email))
          .then(UserRecord.getInstance);

  @override
  Future<UserRecord> getUserByPhoneNumber(String phoneNumber) =>
      handleThenable(jsObject.getUserByPhoneNumber(phoneNumber))
          .then(UserRecord.getInstance);

  @override
  Future<GetUsersResult> getUsers(List<Map<String, String>> identifiers) =>
      handleThenable(jsObject.getUsers(jsify(identifiers)))
          .then((value) => GetUsersResult._fromJsObject(value));

  @override
  Future<auth_interop.UsersResult> importUsers(
      List<auth_interop.UserImportRecord> users,
      [auth_interop.UserImportOptions options]) {
    if (options == null) return handleThenable(jsObject.importUsers(users));
    return handleThenable(jsObject.importUsers(users, options));
  }

  @override
  Future<auth_interop.ListProviderConfigResults> listProviderConfigs(
    auth_interop.AuthProviderConfigFilter options,
  ) =>
      handleThenable(jsObject.listProviderConfigs(options));

  @override
  Future<auth_interop.ListUsersResult> listUsers(
      [int maxResults, String pageToken]) {
    if (maxResults == null) {
      return handleThenable(jsObject.listUsers());
    } else if (maxResults != null) {
      return handleThenable(jsObject.listUsers(maxResults));
    } else {
      return handleThenable(jsObject.listUsers(maxResults, pageToken));
    }
  }

  @override
  Future<void> revokeRefreshTokens(String uid) =>
      handleThenable(jsObject.revokeRefreshTokens(uid));

  @override
  Future<void> setCustomUserClaims(String uid, dynamic customUserClaims) =>
      handleThenable(
          jsObject.setCustomUserClaims(uid, jsify(customUserClaims)));

  @override
  Future<auth_interop.AuthProviderConfig> updateProviderConfig(
    String providerId,
    auth_interop.AuthProviderConfig updatedConfig,
  ) =>
      handleThenable(jsObject.updateProviderConfig(providerId, updatedConfig));

  @override
  Future<UserRecord> updateUser(
          String uid, auth_interop.UpdateRequest properties) =>
      handleThenable(jsObject.updateUser(uid, properties))
          .then(UserRecord.getInstance);

  @override
  Future<Map<String, dynamic>> verifyIdToken(
    String idToken, {
    bool checkRevoked = false,
  }) async {
    final result =
        await handleThenable(jsObject.verifyIdToken(idToken, checkRevoked));
    return dartify(result);
  }

  @override
  Future<Map<String, dynamic>> verifySessionCookie(
    String sessionCookie, {
    bool checkRevoked = false,
  }) async {
    final result = await handleThenable(
        jsObject.verifySessionCookie(sessionCookie, checkRevoked));
    return dartify(result);
  }

  ///Tenant-aware Auth interface used for managing users, configuring SAML/OIDC
  ///providers, generating email links for password reset, email verification,
  ///etc for specific tenants.
  ///
  /// Multi-tenancy support requires Google Cloud's Identity Platform (GCIP). To
  /// learn more about GCIP, including pricing and features, see the GCIP
  /// documentation
  ///
  /// Each tenant contains its own identity providers, settings and sets of
  /// users. Using TenantAwareAuth, users for a specific tenant and
  /// corresponding  OIDC/SAML configurations can also be managed, ID tokens
  /// for users signed in
  /// to a specific tenant can be verified, and email action links can also be
  /// generated for users belonging to the tenant.
  ///
  /// TenantAwareAuth instances for a specific tenantId can be instantiated by
  /// calling auth.tenantManager().authForTenant(tenantId).
  static TenantAwareAuth getInstance(
      auth_interop.TenantAwareAuthJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= TenantAwareAuth._fromJsObject(jsObject);
  }
}

///Defines the tenant manager used to help manage tenant related operations.
///This includes:
///
///- The ability to create, update, list, get and delete tenants for the
///underlying project
///- Getting a `TenantAwareAuth` instance for running Auth related operations
///(user management, provider configuration management, token verification,
///email link generation, etc) in the context of a specified tenant.
class TenantManager extends JsObjectWrapper<auth_interop.TenantManagerJsImpl>
    implements _TenantManager {
  static final _expando = Expando<TenantManager>();

  TenantManager._fromJsObject(auth_interop.TenantManagerJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  TenantAwareAuth authForTenant(String tenantId) =>
      TenantAwareAuth.getInstance(jsObject.authForTenant(tenantId));

  @override
  Future<auth_interop.Tenant> createTenant(
          auth_interop.CreateTenantRequest tenantOptions) =>
      handleThenable(jsObject.createTenant(tenantOptions));
  @override
  Future<void> deleteTenant(String tenantId) =>
      handleThenable(jsObject.deleteTenant(tenantId));
  @override
  Future<auth_interop.Tenant> getTenant(String tenantId) =>
      handleThenable(jsObject.getTenant(tenantId));

  @override
  Future<auth_interop.ListTenantsResult> listTenants([
    num maxResults,
    String pageToken,
  ]) {
    if (pageToken != null) {
      return handleThenable(jsObject.listTenants(maxResults, pageToken));
    } else if (maxResults != null) {
      return handleThenable(jsObject.listTenants(maxResults));
    } else {
      return handleThenable(jsObject.listTenants());
    }
  }

  @override
  Future<auth_interop.Tenant> updateTenant(
    String tenantId,
    auth_interop.UpdateRequest tenantOptions,
  ) =>
      handleThenable(jsObject.updateTenant(tenantId, tenantOptions));

  ///Defines the tenant manager used to help manage tenant related operations.
  ///This includes:
  ///
  ///- The ability to create, update, list, get and delete tenants for the
  ///underlying project
  ///- Getting a `TenantAwareAuth` instance for running Auth related operations
  ///(user management, provider configuration management, token verification,
  ///email link generation, etc) in the context of a specified tenant.
  static TenantManager getInstance(auth_interop.TenantManagerJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= TenantManager._fromJsObject(jsObject);
  }
}

///Interface representing a user.
class UserRecord extends JsObjectWrapper<auth_interop.UserRecordJsImpl>
    implements _UserRecord {
  static final _expando = Expando<UserRecord>();

  UserRecord._fromJsObject(auth_interop.UserRecordJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  Map<String, dynamic> get customClaims => dartify(jsObject.customClaims);

  @override
  bool get disabled => jsObject.disabled;

  @override
  String get displayName => jsObject.displayName;

  @override
  String get email => jsObject.email;

  @override
  bool get emailVerified => jsObject.emailVerified;

  @override
  auth_interop.UserMetadata get metadata => jsObject.metadata;

  @override
  auth_interop.MultiFactorCreateSettings get multiFactor =>
      jsObject.multiFactor;

  @override
  String get passwordHash => jsObject.passwordHash;

  @override
  String get passwordSalt => jsObject.passwordSalt;

  @override
  String get phoneNumber => jsObject.phoneNumber;

  @override
  String get photoURL => jsObject.photoURL;

  @override
  List<auth_interop.UserInfo> get providerData =>
      List.from(jsObject.providerData);

  @override
  String get tenantId => jsObject.tenantId;

  @override
  String get tokensValidAfterTime => jsObject.tokensValidAfterTime;

  @override
  String get uid => jsObject.uid;

  ///Interface representing a user.
  static UserRecord getInstance(auth_interop.UserRecordJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= UserRecord._fromJsObject(jsObject);
  }
}

abstract class _Auth {
  /// Creates a new Firebase custom token (JWT) that can be sent back to a
  /// client device to use to sign in with the client SDKs'
  /// signInWithCustomToken() methods. (Tenant-aware instances will also embed
  /// the tenant ID in the token.)
  ///
  /// See [Create Custom Tokens](https://firebase.google.com/docs/auth/admin/create-custom-tokens) for code samples and detailed documentation.
  Future<String> createCustomToken(
    String uid, [
    Map<String, dynamic> developerClaims,
  ]);

  ///  Returns a promise that resolves with the newly created AuthProviderConfig
  ///  when the new provider configuration is created.
  ///  SAML and OIDC provider support requires Google Cloud's Identity Platform
  ///  (GCIP). To learn more about GCIP, including pricing and features, see the
  ///  [GCIP documentation](https://cloud.google.com/identity-platform).
  Future<auth_interop.AuthProviderConfig> createProviderConfig(
      auth_interop.AuthProviderConfig config);

  /// Creates a new Firebase session cookie with the specified options. The
  /// created JWT string can be set as a server-side session cookie with a
  /// custom cookie policy, and be used for session management. The session
  /// cookie JWT
  /// will have the same payload claims as the provided ID token.
  ///
  /// See [Manage Session Cookies](https://firebase.google.com/docs/auth/admin/manage-cookies) for code samples and detailed documentation.
  Future<String> createSessionCookie(
    String idToken,
    auth_interop.SessionCookieOptions sessionCookieOptions,
  );

  /// Creates a new user.
  ///
  /// See [Create a user](https://firebase.google.com/docs/auth/admin/manage-users#create_a_user) for code samples and detailed documentation.
  Future<UserRecord> createUser(auth_interop.CreateRequest properties);

  /// Deletes the provider configuration corresponding to the provider ID
  /// passed. If the specified ID does not exist, an auth
  /// configuration-not-found error is thrown.
  ///
  /// SAML and OIDC provider support requires Google Cloud's Identity Platform
  /// (GCIP). To learn more about GCIP, including pricing and features, see the
  /// [GCIP documentation](https://cloud.google.com/identity-platform).
  Future<void> deleteProviderConfig(String providerId);

  /// Deletes an existing user.
  ///
  /// See (Delete a user)[https://firebase.google.com/docs/auth/admin/manage-users#delete_a_user] for code samples and detailed documentation.
  Future<void> deleteUser(String uid);

  /// Generates the out of band email action link to verify the user's ownership
  /// of the specified email. The ActionCodeSettings object provided as an
  /// argument to this method defines whether the link is to be handled by a
  /// mobile app or browser along with additional state information to be passed
  /// in the deep link, etc.
  Future<String> generateEmailVerificationLink(
    String email, [
    auth_interop.ActionCodeSettings actionCodeSettings,
  ]);

  /// Generates the out of band email action link to reset a user's password.
  /// The link is generated for the user with the specified email address. The
  /// optional `ActionCodeSettings` object defines whether the link is to be
  /// handled by a mobile app or browser and the additional state information to
  /// be passed in the deep link, etc.
  Future<String> generatePasswordResetLink(
    String email, [
    auth_interop.ActionCodeSettings actionCodeSettings,
  ]);

  /// Generates the out of band email action link to sign in or sign up the
  /// owner of the specified email. The ActionCodeSettings object provided as an
  /// argument to this method defines whether the link is to be handled by a
  /// mobile app or browser along with additional state information to be passed
  /// in the deep link, etc.
  Future<String> generateSignInWithEmailLink(
    String email, [
    auth_interop.ActionCodeSettings actionCodeSettings,
  ]);

  /// Looks up an Auth provider configuration by the provided ID. Returns a
  /// promise that resolves with the provider configuration corresponding to the
  /// provider ID specified. If the specified ID does not exist, an auth/
  /// configuration-not-found error is thrown.
  ///
  /// SAML and OIDC provider support requires Google Cloud's Identity Platform
  /// (GCIP). To learn more about GCIP, including pricing and features, see the
  /// [GCIP documentation](https://cloud.google.com/identity-platform).
  Future<auth_interop.AuthProviderConfig> getProviderConfig(String providerId);

  /// Gets the user data for the user corresponding to a given uid.
  ///
  /// See [Retrieve user data](https://firebase.google.com/docs/auth/admin/manage-users#retrieve_user_data) for code samples and detailed documentation.
  Future<UserRecord> getUser(String uid);

  /// Gets the user data for the user corresponding to a given email.
  ///
  /// See [Retrieve user data](https://firebase.google.com/docs/auth/admin/manage-users#retrieve_user_data) for code samples and detailed documentation.
  Future<UserRecord> getUserByEmail(String uid);

  /// Gets the user data for the user corresponding to a given phone number. The
  /// phone number has to conform to the E.164 specification.
  ///
  /// See [Retrieve user data](https://firebase.google.com/docs/auth/admin/manage-users#retrieve_user_data) for code samples and detailed documentation.
  Future<UserRecord> getUserByPhoneNumber(String uid);

  /// Imports the provided list of users into Firebase Auth. A maximum of 1000
  /// users are allowed to be imported one at a time. When importing users with
  /// passwords, UserImportOptions are required to be specified. This operation
  /// is optimized for bulk imports and will ignore checks on uid, email and
  /// other identifier uniqueness which could result in duplications.
  Future<auth_interop.UsersResult> importUsers(
    List<auth_interop.UserImportRecord> users, [
    auth_interop.UserImportOptions options,
  ]);

  /// Returns the list of existing provider configurations matching the filter
  /// provided. At most, 100 provider configs can be listed at a time.
  ///
  /// SAML and OIDC provider support requires Google Cloud's Identity Platform
  /// (GCIP). To learn more about GCIP, including pricing and features, see the
  /// [GCIP documentation](https://cloud.google.com/identity-platform).
  Future<auth_interop.ListProviderConfigResults> listProviderConfigs(
      auth_interop.AuthProviderConfigFilter options);

  /// Retrieves a list of users (single batch only) with a size of maxResults
  /// starting from the offset as specified by pageToken. This is used to
  /// retrieve all the users of a specified project in batches.
  ///
  /// See [List all users](https://firebase.google.com/docs/auth/admin/manage-users#list_all_users) for code samples and detailed documentation.
  Future<auth_interop.ListUsersResult> listUsers(
      [int maxResults, String pageToken]);

  /// Revokes all refresh tokens for an existing user.
  ///
  /// This API will update the user's {@link admin.auth.
  /// UserRecord#tokensValidAfterTime tokensValidAfterTime} to the current UTC.
  /// It is important that the server on which this is called has its clock set
  /// correctly and synchronized.
  ///
  /// While this will revoke all sessions for a specified user and disable any
  /// new ID tokens for existing sessions from getting minted, existing ID
  /// tokens may remain active until their natural expiration (one hour). To
  /// verify that ID tokens are revoked, use {@link admin.auth.
  /// Auth#verifyIdToken verifyIdToken(idToken, true)} where checkRevoked is set
  /// to true.
  Future<void> revokeRefreshTokens(String uid);

  /// Sets additional developer claims on an existing user identified by the
  /// provided uid, typically used to define user roles and levels of access.
  /// These claims should propagate to all devices where the user is already
  /// signed in (after token expiration or when token refresh is forced) and the
  /// next time the user signs in. If a reserved OIDC claim name is used (sub,
  /// iat, iss, etc), an error is thrown. They are set on the authenticated
  /// user's ID token JWT.
  ///
  /// See [Defining user roles and access levels](https://firebase.google.com/docs/auth/admin/custom-claims) for code samples and detailed
  /// documentation.
  Future<void> setCustomUserClaims(
    String uid,
    Map<String, dynamic> customUserClaims,
  );

  /// The tenant manager instance associated with the current project.
  TenantManager tenantManager();

  /// Returns a promise that resolves with the updated AuthProviderConfig corresponding to the provider ID specified. If the specified ID does not exist, an auth/configuration-not-found error is thrown.
  ///
  /// SAML and OIDC provider support requires Google Cloud's Identity Platform
  /// (GCIP). To learn more about GCIP, including pricing and features, see the
  /// [GCIP documentation](https://cloud.google.com/identity-platform).
  Future<auth_interop.AuthProviderConfig> updateProviderConfig(
    String providerId,
    /*OIDCUpdateAuthProviderRequest | SAMLUpdateAuthProviderRequest */
    dynamic updatedConfig,
  );

  /// Updates an existing user.
  ///
  /// See [Update a user](https://firebase.google.com/docs/auth/admin/manage-users#update_a_user) for code samples and detailed documentation.
  Future<UserRecord> updateUser(
      String uid, auth_interop.UpdateRequest properties);

  /// Verifies a Firebase ID token (JWT). If the token is valid, the promise is
  /// fulfilled with the token's decoded claims; otherwise, the promise is
  /// rejected. An optional flag can be passed to additionally check whether the
  /// ID token was revoked.
  ///
  /// See [Verify ID Tokens](https://firebase.google.com/docs/auth/admin/verify-id-tokens) for code samples and detailed documentation.
  Future<Map<String, dynamic>> verifyIdToken(
    String idToken, {
    bool checkRevoked = false,
  });

  /// Verifies a Firebase session cookie. Returns a Promise with the cookie
  /// claims. Rejects the promise if the cookie could not be verified. If
  /// checkRevoked is set to true, verifies if the session corresponding to the
  /// session cookie was revoked. If the corresponding user's session was
  /// revoked, an auth/session-cookie-revoked error is thrown. If not specified
  /// the check is not performed.
  ///
  /// See [Verify Session Cookies](https://firebase.google.com/docs/auth/admin/manage-cookies#verify_session_cookie_and_check_permissions) for code samples and detailed documentation
  Future<Map<String, dynamic>> verifySessionCookie(
    String sesionCookie, {
    bool checkForRevocation = false,
  });
}

/// Tenant-aware Auth interface used for managing users, configuring SAML/OIDC
/// providers, generating email links for password reset, email verification,
/// etc for specific tenants.
///
/// Multi-tenancy support requires Google Cloud's Identity Platform (GCIP). To
/// learn more about GCIP, including pricing and features, see the GCIP
/// documentation
///
/// Each tenant contains its own identity providers, settings and sets of
/// users. Using TenantAwareAuth, users for a specific tenant and corresponding
/// OIDC/SAML configurations can also be managed, ID tokens for users signed in
/// to a specific tenant can be verified, and email action links can also be
/// generated for users belonging to the tenant.
///
/// TenantAwareAuth instances for a specific tenantId can be instantiated by
/// calling auth.tenantManager().authForTenant(tenantId).
abstract class _TenantAwareAuth {
  /// The tenant identifier corresponding to this TenantAwareAuth instance. All
  /// calls to the user management APIs, OIDC/SAML provider management APIs,
  /// email link generation APIs, etc will only be applied within the scope of
  /// this tenant.
  String get tenantId;

  /// Creates a new Firebase custom token (JWT) that can be sent back to a
  /// client device to use to sign in with the client SDKs'
  /// signInWithCustomToken() methods. (Tenant-aware instances will also embed
  /// the tenant ID in the token.)
  ///
  /// See [Create Custom Tokens](https://firebase.google.com/docs/auth/admin/
  /// create-custom-tokens) for code samples and detailed documentation.
  ///
  /// Parameters
  /// - `uid`:
  /// The uid to use as the custom token's subject.
  ///
  /// - `developerClaims`:
  /// Optional additional claims to include in the custom token's payload.
  ///
  /// Returns `Future<String>`
  /// A promise fulfilled with a custom token for the provided uid and payload.
  Future<String> createCustomToken(
    String uid, [
    Map<String, dynamic> developerClaims,
  ]);

  /// Returns a promise that resolves with the newly created AuthProviderConfig
  /// when the new provider configuration is created.
  ///
  /// SAML and OIDC provider support requires Google Cloud's Identity Platform
  /// (GCIP). To learn more about GCIP, including pricing and features, see the
  /// [GCIP documentation](https://cloud.google.com/identity-platform).
  ///
  /// Parameters
  /// - `config`:
  /// The provider configuration to create.
  ///
  /// Returns `Future<AuthProviderConfig>`
  /// A promise that resolves with the created provider configuration.
  Future<auth_interop.AuthProviderConfig> createProviderConfig(
    auth_interop.AuthProviderConfig config,
  );

  /// Creates a new Firebase session cookie with the specified options. The
  /// created JWT string can be set as a server-side session cookie with a
  /// custom cookie policy, and be used for session management. The session
  /// cookie JWT will have the same payload claims as the provided ID token.
  ///
  /// See [Manage Session Cookies](https://firebase.google.com/docs/auth/admin/manage-cookies) for code samples and detailed documentation.
  ///
  /// Parameters
  /// - `idToken`:
  /// The Firebase ID token to exchange for a session cookie.
  ///
  /// - `sessionCookieOptions`:
  /// The session cookie options which includes custom session duration.
  ///
  /// Returns `Future<string>`
  /// A promise that resolves on success with the created session cookie.

  Future<String> createSessionCookie(
    String idToken,
    auth_interop.SessionCookieOptions sessionCookieOptions,
  );

  /// Creates a new user.
  ///
  /// See [Create a user](https://firebase.google.com/docs/auth/admin/manage-users#create_a_user) for code samples and detailed documentation.
  ///
  /// Parameters
  /// - `properties`:
  /// The properties to set on the new user record to be created.
  ///
  /// Returns Future<UserRecord>
  /// A promise fulfilled with the user data corresponding to the newly created
  /// user.
  Future<UserRecord> createUser(auth_interop.CreateRequest properties);

  /// Deletes the provider configuration corresponding to the provider ID
  /// passed. If the specified ID does not exist, an auth/
  /// configuration-not-found error is thrown.
  ///
  /// SAML and OIDC provider support requires Google Cloud's Identity Platform
  /// (GCIP). To learn more about GCIP, including pricing and features, see the
  /// [GCIP documentation](https://cloud.google.com/identity-platform).
  ///
  /// Parameters
  /// `providerId`:
  /// The provider ID corresponding to the provider config to delete.
  ///
  /// Returns `Future<void>`
  /// A promise that resolves on completion.
  Future<void> deleteProviderConfig(String providerId);

  /// Deletes an existing user.
  ///
  /// See [Delete a user](https://firebase.google.com/docs/auth/admin/manage-users#delete_a_user) for code samples and detailed documentation.
  ///
  /// Parameters
  /// - `uid`:
  /// The uid corresponding to the user to delete.
  ///
  /// Returns `Future<void>`
  /// An empty promise fulfilled once the user has been deleted.
  Future<void> deleteUser(String uid);

  /// Deletes the users specified by the given uids.
  ///
  /// Deleting a non-existing user won't generate an error (i.e. this method is
  /// idempotent.) Non-existing users are considered to be successfully
  /// deleted, and are therefore counted in the DeleteUsersResult.successCount
  /// value.
  ///
  /// Only a maximum of 1000 identifiers may be supplied. If more than 1000
  /// identifiers are supplied, this method throws a FirebaseAuthError.
  ///
  /// This API is currently rate limited at the server to 1 QPS. If you exceed
  /// this, you may get a quota exceeded error. Therefore, if you want to
  /// delete more than 1000 users, you may need to add a delay to ensure you
  /// don't go over this limit.
  ///
  /// Parameters
  /// - `uids`:
  /// The uids corresponding to the users to delete.
  ///
  /// Returns `Future<DeleteUsersResult>`
  /// A Promise that resolves to the total number of successful/failed
  /// deletions, as well as the array of errors that corresponds to the failed
  /// deletions.
  Future<auth_interop.UsersResult> deleteUsers(List<String> uids);

  /// Generates the out of band email action link to verify the user's
  /// ownership of the specified email. The ActionCodeSettings object provided
  /// as an argument to this method defines whether the link is to be handled
  /// by a mobile app or browser along with additional state information to be
  /// passed in the deep link, etc.
  ///
  /// Parameters
  /// - `email`:
  /// The email account to verify.
  ///
  /// Optional actionCodeSettings: ActionCodeSettings
  /// The action code settings. If specified, the state/continue URL is set as
  /// the "continueUrl" parameter in the email verification link. The default
  /// email verification landing page will use this to display a link to go
  /// back to the app if it is installed. If the actionCodeSettings is not
  /// specified, no URL is appended to the action URL. The state URL provided
  /// must belong to a domain that is whitelisted by the developer in the
  /// console. Otherwise an error is thrown. Mobile app redirects are only
  /// applicable if the developer configures and accepts the Firebase Dynamic
  /// Links terms of service. The Android package name and iOS bundle ID are
  /// respected only if they are configured in the same Firebase Auth project.
  ///
  /// Returns `Future<string>`
  /// A promise that resolves with the generated link.
  Future<String> generateEmailVerificationLink(
    String email, [
    auth_interop.ActionCodeSettings actionCodeSettings,
  ]);

  /// Generates the out of band email action link to reset a user's password.
  /// The link is generated for the user with the specified email address. The
  /// optional ActionCodeSettings object defines whether the link is to be
  /// handled by a mobile app or browser and the additional state information
  /// to be passed in the deep link, etc.
  ///
  /// Parameters
  /// - `email`:
  /// The email address of the user whose password is to be reset.
  ///
  /// - `actionCodeSettings`:
  /// The action code settings. If specified, the state/continue URL is set as
  /// the "continueUrl" parameter in the password reset link. The default
  /// password reset landing page will use this to display a link to go back to
  /// the app if it is installed. If the actionCodeSettings is not specified,
  /// no URL is appended to the action URL. The state URL provided must belong
  /// to a domain that is whitelisted by the developer in the console.
  /// Otherwise an error is thrown. Mobile app redirects are only applicable if
  /// the developer configures and accepts the Firebase Dynamic Links terms of
  /// service. The Android package name and iOS bundle ID are respected only if
  /// they are configured in the same Firebase Auth project.
  Future<String> generatePasswordResetLink(
    String email, [
    auth_interop.ActionCodeSettings actionCodeSettings,
  ]);

  /// Generates the out of band email action link to sign in or sign up the
  /// owner of the specified email. The `ActionCodeSettings` object provided as
  /// an argument to this method defines whether the link is to be handled by a
  /// mobile app or browser along with additional state information to be
  /// passed in the deep link, etc.
  ///
  /// Parameters
  /// - `email`:
  /// The email account to sign in with.
  ///
  /// - `actionCodeSettings`:
  /// The action code settings. These settings provide Firebase with
  /// instructions on how to construct the email link. This includes the sign
  /// in completion URL or the deep link for redirects and the mobile apps to
  /// use when the sign-in link is opened on an Android or iOS device. Mobile
  /// app redirects are only applicable if the developer configures and accepts
  /// the Firebase Dynamic Links terms of service. The Android package name and
  /// iOS bundle ID are respected only if they are configured in the same
  /// Firebase Auth project.
  ///
  /// Returns `Future<string>`
  /// A promise that resolves with the generated link.
  Future<String> generateSignInWithEmailLink(
    String email, [
    auth_interop.ActionCodeSettings actionCodeSettings,
  ]);

  /// Looks up an Auth provider configuration by the provided ID. Returns a
  /// promise that resolves with the provider configuration corresponding to
  /// the provider ID specified. If the specified ID does not exist, an auth/
  /// configuration-not-found error is thrown.
  ///
  /// SAML and OIDC provider support requires Google Cloud's Identity Platform
  /// (GCIP). To learn more about GCIP, including pricing and features, see the
  /// [GCIP documentation](https://cloud.google.com/identity-platform).
  ///
  /// Parameters
  /// - `providerId`:
  /// The provider ID corresponding to the provider config to return.
  ///
  /// Returns `Future<AuthProviderConfig>`
  /// A promise that resolves with the configuration corresponding to the
  /// provided ID.
  Future<auth_interop.AuthProviderConfig> getProviderConfig(String providerid);

  /// Gets the user data for the user corresponding to a given uid.
  ///
  /// See [Retrieve user data](https://firebase.google.com/docs/auth/admin/manage-users#retrieve_user_data) for code samples and detailed documentation.
  ///
  /// Parameters
  /// - `uid`:
  /// The uid corresponding to the user whose data to fetch.
  ///
  /// Returns Future<UserRecord>
  /// A promise fulfilled with the user data corresponding to the provided uid.
  Future<UserRecord> getUser(String uid);

  /// Gets the user data for the user corresponding to a given email.
  ///
  /// See [Retrieve user data](https://firebase.google.com/docs/auth/admin/manage-users#retrieve_user_data) for code samples and detailed documentation.
  ///
  /// Parameters
  /// - `email`:
  /// The email corresponding to the user whose data to fetch.
  ///
  /// Returns `Future<UserRecord>`
  /// A promise fulfilled with the user data corresponding to the provided
  /// email.
  Future<UserRecord> getUserByEmail(String email);

  /// Gets the user data for the user corresponding to a given phone number.
  /// The phone number has to conform to the E.164 specification.
  ///
  /// See [Retrieve user data](https://firebase.google.com/docs/auth/admin/manage-users#retrieve_user_data) for code samples and detailed documentation.
  ///
  /// Parameters
  /// - `phoneNumber`:
  /// The phone number corresponding to the user whose data to fetch.
  ///
  /// Returns Future<UserRecord>
  /// A promise fulfilled with the user data corresponding to the provided
  /// phone number.
  Future<UserRecord> getUserByPhoneNumber(String phoneNumber);

  /// Gets the user data corresponding to the specified identifiers.
  ///
  /// There are no ordering guarantees; in particular, the nth entry in the
  /// result list is not guaranteed to correspond to the nth entry in the input
  /// parameters list.
  ///
  /// Only a maximum of 100 identifiers may be supplied. If more than 100
  /// identifiers are supplied, this method throws a FirebaseAuthError.
  ///
  /// throws
  /// FirebaseAuthError If any of the identifiers are invalid or if more than
  /// 100 identifiers are specified.
  ///
  /// Parameters
  /// - `identifiers`: List<Map<String,String>>
  /// The identifiers used to indicate which user records should be returned.
  /// Must have <= 100 entries. Valid keys are 'uid', 'email', 'phoneNumber',
  /// 'providerId', 'providerUid'
  ///
  /// Example:
  ///
  /// ```
  /// final app = initializeApp();
  /// final auth = app.auth();
  /// final tenantManager = auth.tenantManager();
  /// final tenantAwareAuth = tenantManager.authForTenant('some_tenant_id');
  /// final identifiers = [{'uid': 'some_id'}, {'email':'some@email.example'}];
  /// final userResults = await tenantAwareAuth.getUsers(identifiers);
  /// ```
  ///
  ///
  /// Returns `Future<GetUsersResult>`
  /// A promise that resolves to the corresponding user records.
  ///
  /// See https://firebase.google.com/docs/reference/admin/node/admin.auth.TenantAwareAuth#getusers
  Future<GetUsersResult> getUsers(List<Map<String, String>> identifiers);

  /// Imports the provided list of users into Firebase Auth. A maximum of 1000
  /// users are allowed to be imported one at a time. When importing users with
  /// passwords, UserImportOptions are required to be specified. This operation
  /// is optimized for bulk imports and will ignore checks on uid, email and
  /// other identifier uniqueness which could result in duplications.
  ///
  /// Parameters
  /// - `users`:
  /// The list of user records to import to Firebase Auth.
  ///
  /// - `options`:
  /// The user import options, required when the users provided include
  /// password credentials.
  ///
  /// Returns `Future<UserImportResult>`
  /// A promise that resolves when the operation completes with the result of
  /// the import. This includes the number of successful imports, the number of
  /// failed imports and their corresponding errors.
  Future<auth_interop.UsersResult> importUsers(
    List<auth_interop.UserImportRecord> users, [
    auth_interop.UserImportOptions options,
  ]);

  /// Returns the list of existing provider configurations matching the filter
  /// provided. At most, 100 provider configs can be listed at a time.
  ///
  /// SAML and OIDC provider support requires Google Cloud's Identity Platform
  /// (GCIP). To learn more about GCIP, including pricing and features, see the
  /// [GCIP documentation](https://cloud.google.com/identity-platform).
  ///
  /// Parameters
  /// - `options`:
  /// The provider config filter to apply.
  ///
  /// Returns Future<ListProviderConfigResults>
  /// A promise that resolves with the list of provider configs meeting the
  /// filter requirements.
  Future<auth_interop.ListProviderConfigResults> listProviderConfigs(
    auth_interop.AuthProviderConfigFilter options,
  );

  /// Retrieves a list of users (single batch only) with a size of maxResults
  /// starting from the offset as specified by pageToken. This is used to
  /// retrieve all the users of a specified project in batches.
  ///
  /// See [List all users](https://firebase.google.com/docs/auth/admin/manage-users#list_all_users) for code samples and detailed documentation.
  ///
  /// Parameters
  /// - `maxResults`:
  /// The page size, 1000 if undefined. This is also the maximum allowed limit.
  ///
  /// - `pageToken`:
  /// The next page token. If not specified, returns users starting without any
  /// offset.
  ///
  /// Returns `Future<ListUsersResult>`
  /// A promise that resolves with the current batch of downloaded users and
  /// the next page token.
  ///
  Future<auth_interop.ListUsersResult> listUsers(
      [int maxResults, String pageToken]);

  /// Revokes all refresh tokens for an existing user.
  ///
  /// This API will update the user's to the current UTC.
  /// It is important that the server on which this is called has its clock set
  /// correctly and synchronized.
  ///
  /// While this will revoke all sessions for a specified user and disable any
  /// new ID tokens for existing sessions from getting minted, existing ID
  /// tokens may remain active until their natural expiration (one hour). To
  /// verify that ID tokens are revoked, use {@link admin.auth.
  /// Auth#verifyIdToken verifyIdToken(idToken, true)} where checkRevoked is
  /// set to true.
  ///
  /// Parameters
  /// - `uid`:
  /// The uid corresponding to the user whose refresh tokens are to be revoked.
  ///
  /// Returns `Future<void>`
  /// An empty promise fulfilled once the user's refresh tokens have been
  /// revoked.
  Future<void> revokeRefreshTokens(String uid);

  /// Sets additional developer claims on an existing user identified by the
  /// provided uid, typically used to define user roles and levels of access.
  /// These claims should propagate to all devices where the user is already
  /// signed in (after token expiration or when token refresh is forced) and
  /// the next time the user signs in. If a reserved OIDC claim name is used
  /// (sub, iat, iss, etc), an error is thrown. They are set on the
  /// authenticated user's ID token JWT.
  ///
  /// See [Defining user roles and access levels](https://firebase.google.com/docs/auth/admin/custom-claims) for code samples and detailed documentation.
  ///
  /// Parameters
  /// - `uid`:
  /// The uid of the user to edit.
  ///
  /// - `customUserClaims`:
  /// The developer claims to set. If null is passed, existing custom claims
  /// are deleted. Passing a custom claims payload larger than 1000 bytes will
  /// throw an error. Custom claims are added to the user's ID token which is
  /// transmitted on every authenticated request. For profile non-access
  /// related user attributes, use database or other separate storage systems.
  ///
  /// Returns `Future<void>`
  /// A promise that resolves when the operation completes successfully.
  Future<void> setCustomUserClaims(
    String uid,
    Map<String, dynamic> customUserClaims,
  );

  /// Returns a promise that resolves with the updated AuthProviderConfig
  /// corresponding to the provider ID specified. If the specified ID does not
  /// exist, an auth/configuration-not-found error is thrown.
  ///
  /// SAML and OIDC provider support requires Google Cloud's Identity Platform
  /// (GCIP). To learn more about GCIP, including pricing and features, see the
  /// GCIP documentation.
  ///
  /// Parameters
  /// - `providerId`:
  /// The provider ID corresponding to the provider config to update.
  ///
  /// - `updatedConfig`:
  /// The updated configuration.
  ///
  /// Returns `Future<AuthProviderConfig>`
  /// A promise that resolves with the updated provider configuration.
  Future<auth_interop.AuthProviderConfig> updateProviderConfig(
    String providerId,
    auth_interop.AuthProviderConfig updatedConfig,
  );

  /// Updates an existing user.
  ///
  /// See [Update a user](https://firebase.google.com/docs/auth/admin/manage-users#update_a_user) for code samples and detailed documentation.
  ///
  /// Parameters
  /// - `uid`:
  /// The uid corresponding to the user to delete.
  ///
  /// - `properties`:
  /// The properties to update on the provided user.
  ///
  /// Returns `Future<UserRecord>`
  /// A promise fulfilled with the updated user data.
  Future<UserRecord> updateUser(
      String uid, auth_interop.UpdateRequest properties);

  /// Verifies a Firebase ID token (JWT). If the token is valid, the promise is
  /// fulfilled with the token's decoded claims; otherwise, the promise is
  /// rejected. An optional flag can be passed to additionally check whether
  /// the ID token was revoked.
  ///
  /// See [Verify ID Tokens](https://firebase.google.com/docs/auth/admin/verify-id-tokens) for code samples and detailed documentation.
  ///
  /// Parameters
  /// - `idToken`:
  /// The ID token to verify.
  ///
  /// - `checkRevoked`:
  /// Whether to check if the ID token was revoked. This requires an extra
  /// request to the Firebase Auth backend to check the tokensValidAfterTime
  /// time for the corresponding user. When not specified, this additional
  /// check is not applied.
  ///
  /// Returns `Future<DecodedIdToken>`
  /// A promise fulfilled with the token's decoded claims if the ID token is
  /// valid; otherwise, a rejected promise.
  Future<Map<String, dynamic>> verifyIdToken(
    String idToken, {
    bool checkRevoked = false,
  });

  /// Verifies a Firebase session cookie. Returns a Promise with the cookie
  /// claims. Rejects the promise if the cookie could not be verified. If
  /// checkRevoked is set to true, verifies if the session corresponding to the
  /// session cookie was revoked. If the corresponding user's session was
  /// revoked, an auth/session-cookie-revoked error is thrown. If not specified
  /// the check is not performed.
  ///
  /// See [Verify Session Cookies](https://firebase.google.com/docs/auth/admin/manage-cookies#verify_session_cookie_and_check_permissions) for code samples and detailed documentation
  ///
  /// Parameters
  /// - `sessionCookie`: string
  /// The session cookie to verify.
  ///
  /// - `checkForRevocation`:
  /// Whether to check if the session cookie was revoked. This requires an
  /// extra request to the Firebase Auth backend to check the
  /// tokensValidAfterTime time for the corresponding user. When not specified,
  /// this additional check is not performed.
  ///
  /// Returns `Future<DecodedIdToken>`
  /// A promise fulfilled with the session cookie's decoded claims if the
  /// session cookie is valid; otherwise, a rejected promise.
  Future<Map<String, dynamic>> verifySessionCookie(
    String sessionCookie, {
    bool checkRevoked = false,
  });
}

/// Defines the tenant manager used to help manage tenant related operations.
/// This includes:
///
/// - The ability to create, update, list, get and delete tenants for the
/// underlying project
/// - Getting a `TenantAwareAuth` instance for running Auth related operations
/// (user management, provider configuration management, token verification,
/// email link generation, etc) in the context of a specified tenant.
abstract class _TenantManager {
  ///  Parameters
  ///
  /// `tenantId`:
  /// The tenant ID whose TenantAwareAuth instance is to be returned
  ///
  /// Returns TenantAwareAuth
  /// The TenantAwareAuth instance corresponding to this tenant identifier.
  TenantAwareAuth authForTenant(String tenantId);

  /// Creates a new tenant. When creating new tenants, tenants that use
  /// separate billing and quota will require their own project and must be
  /// defined as full_service.
  ///
  /// Parameters
  ///
  /// `tenantOptions`:
  /// The properties to set on the new tenant configuration to be created.
  ///
  /// Returns Future<Tenant>
  /// A promise fulfilled with the tenant configuration corresponding to the
  /// newly created tenant.
  Future<auth_interop.Tenant> createTenant(
    auth_interop.CreateTenantRequest tenantOptions,
  );

  /// Deletes an existing tenant.
  /// Parameters
  ///
  /// `tenantId`:
  /// The tenantId corresponding to the tenant to delete.
  ///
  /// Returns Future<void>
  /// An empty promise fulfilled once the tenant has been deleted.
  Future<void> deleteTenant(String tenantId);

  /// Gets the tenant configuration for the tenant corresponding to a given
  /// tenantId.
  ///
  /// Parameters
  ///
  /// `tenantId`:
  /// The tenant identifier corresponding to the tenant whose data to fetch.
  ///
  /// Returns Future<Tenant>
  /// A promise fulfilled with the tenant configuration to the provided
  /// tenantId.
  Future<auth_interop.Tenant> getTenant(String tenantId);

  /// Retrieves a list of tenants (single batch only) with a size of maxResults
  /// starting from the offset as specified by pageToken. This is used to
  /// retrieve all the tenants of a specified project in batches.
  ///
  /// Parameters
  /// - `maxResults`:
  /// The page size, 1000 if undefined. This is also the maximum allowed limit.
  ///
  /// - `pageToken`:
  /// The next page token. If not specified, returns tenants starting without
  /// any offset.
  ///
  /// Returns Future<ListTenantsResult>
  /// A promise that resolves with a batch of downloaded tenants and the next
  /// page token.
  Future<auth_interop.ListTenantsResult> listTenants([
    num maxResults,
    String pageToken,
  ]);

  /// Updates an existing tenant configuration.
  ///
  /// Parameters
  /// - `tenantId`:
  /// The tenantId corresponding to the tenant to delete.
  ///
  /// tenantOptions: UpdateTenantRequest
  /// The properties to update on the provided tenant.
  ///
  /// Returns Future<Tenant>
  /// A promise fulfilled with the update tenant data.
  Future<auth_interop.Tenant> updateTenant(
    String tenantId,
    auth_interop.UpdateRequest tenantOptions,
  );
}

abstract class _UserRecord {
  /// The user's custom claims object if available, typically used to define
  /// user roles and propagated to an authenticated user's ID token. This is set
  /// via setCustomUserClaims()
  Map<String, dynamic> get customClaims;

  /// Whether or not the user is disabled: true for disabled; false for enabled.
  bool get disabled;

  /// The user's display name.
  String get displayName;

  /// The user's primary email, if set.
  String get email;

  /// Whether or not the user's primary email is verified.
  bool get emailVerified;

  /// Additional metadata about the user.
  auth_interop.UserMetadata get metadata;

  /// The multi-factor related properties for the current user, if available.
  auth_interop.MultiFactorCreateSettings get multiFactor;

  /// The user's hashed password (base64-encoded), only if Firebase Auth hashing
  /// algorithm (SCRYPT) is used. If a different hashing algorithm had been used
  /// when uploading this user, as is typical when migrating from another Auth
  /// system, this will be an empty string. If no password is set, this is null.
  /// This is only available when the user is obtained from listUsers().
  String get passwordHash;

  /// The user's password salt (base64-encoded), only if Firebase Auth hashing
  /// algorithm (SCRYPT) is used. If a different hashing algorithm had been used
  /// to upload this user, typical when migrating from another Auth system, this
  /// will be an empty string. If no password is set, this is null. This is only
  /// available when the user is obtained from listUsers().
  String get passwordSalt;

  /// The user's primary phone number, if set.
  String get phoneNumber;

  /// The user's photo URL.
  String get photoURL;

  /// An array of providers (for example, Google, Facebook) linked to the user.
  List<auth_interop.UserInfo> get providerData;

  /// The ID of the tenant the user belongs to, if available.
  String get tenantId;

  /// The date the user's tokens are valid after, formatted as a UTC string.
  /// This is updated every time the user's refresh token are revoked either
  /// from the revokeRefreshTokens() API or from the Firebase Auth backend on
  /// big account changes (password resets, password or email updates, etc).
  String get tokensValidAfterTime;

  /// The user's uid.
  String get uid;
}

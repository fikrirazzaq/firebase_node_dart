part of '../../admin.dart';

/// Instance of admin.credential
const credential = CredentialBuilder();

/// A (read-only) array of all the initialized Apps.
///
/// See: <https://firebase.google.com/docs/reference/admin/node/admin#.apps>.
List<App> get apps => admin_interop.apps
    // explicitly typing the param as dynamic to work-around
    // https://github.com/dart-lang/sdk/issues/33537
    // ignore: unnecessary_lambdas
    .map((dynamic e) => App.getInstance(e))
    .toList();

/// Retrieves an instance of an [App].
///
/// With no arguments, this returns the default [App]. With a single
/// string argument, it returns the named App.
///
/// This function throws an exception if the app you are trying
/// to access does not exist.
///
/// See: <https://firebase.google.com/docs/reference/admin/node/admin.app>.
App app([String name]) {
  var jsObject = (name != null) ? admin_interop.app(name) : admin_interop.app();
  return App.getInstance(jsObject);
}

/// Gets the [Auth] object for the default [App] or a given App.
///
/// See: <https://firebase.google.com/docs/reference/admin/node/admin.auth>.
Auth auth([App app]) {
  var jsObject =
      (app != null) ? admin_interop.auth(app.jsObject) : admin_interop.auth();
  return Auth.getInstance(jsObject);
}

/// Accesses the [Database] service for the default [App] or a given app.
///
/// See: <https://firebase.google.com/docs/reference/admin/node/admin.database>.
Database database([App app]) {
  var jsObject = (app != null)
      ? admin_interop.database(app.jsObject)
      : admin_interop.database();
  return Database.getInstance(jsObject);
}

/// Accesses the [Firestore] service for the default [App] or a given app.
///
/// See: <https://firebase.google.com/docs/reference/admin/node/admin.firestore>.
Firestore firestore([App app]) {
  var jsObject = (app != null)
      ? admin_interop.firestore(app.jsObject)
      : admin_interop.firestore();
  return Firestore.getInstance(jsObject);
}

/// Creates (and initializes) a Firebase [App] with API key, auth domain,
/// database URL and storage bucket.
///
/// See: <https://firebase.google.com/docs/reference/admin/node/admin#initializeapp>.
App initializeApp({
  String name = '[DEFAULT]',
  AppOptions options,
}) =>
    App.getInstance(admin_interop.initializeApp(
      options?.jsObject ??
          AppOptions.getInstance(admin_interop.AppOptionsJsImpl()).jsObject,
      name,
    ));

/// Accesses the [InstanceId] service for the default [App] or a given app.
///
/// See: <https://firebase.google.com/docs/reference/admin/node/admin.instanceId>.
InstanceId instanceId([App app]) {
  var jsObject = (app != null)
      ? admin_interop.instanceId(app.jsObject)
      : admin_interop.instanceId();
  return InstanceId.getInstance(jsObject);
}

/// Accesses the [MachineLearning] service for the default [App] or a given app.
///
/// See: <https://firebase.google.com/docs/reference/admin/node/admin.machineLearning>.
MachineLearning machineLearning([App app]) {
  var jsObject = (app != null)
      ? admin_interop.machineLearning(app.jsObject)
      : admin_interop.machineLearning();
  return MachineLearning.getInstance(jsObject);
}

/// Accesses the [Messaging] service for the default [App] or a given app.
///
/// See: <https://firebase.google.com/docs/reference/admin/node/admin.messaging>.
Messaging messaging([App app]) {
  var jsObject = (app != null)
      ? admin_interop.messaging(app.jsObject)
      : admin_interop.messaging();
  return Messaging.getInstance(jsObject);
}

/// Accesses the [ProjectManagement] service for the default [App] or a given
/// app.
///
/// See: <https://firebase.google.com/docs/reference/admin/node/admin.projectManagement>.
ProjectManagement projectManagement([App app]) {
  var jsObject = (app != null)
      ? admin_interop.projectManagement(app.jsObject)
      : admin_interop.projectManagement();
  return ProjectManagement.getInstance(jsObject);
}

/// Accesses the [RemoteConfig] service for the default [App] or a given app.
///
/// See: <https://firebase.google.com/docs/reference/admin/node/admin.remoteConfig>.
RemoteConfig remoteConfig([App app]) {
  var jsObject = (app != null)
      ? admin_interop.remoteConfig(app.jsObject)
      : admin_interop.remoteConfig();
  return RemoteConfig.getInstance(jsObject);
}

/// Accesses the [Storage] service for the default [App] or a given app.
///
/// See: <https://firebase.google.com/docs/reference/admin/node/admin.storage>.
Storage storage([App app]) {
  var jsObject = (app != null)
      ? admin_interop.storage(app.jsObject)
      : admin_interop.storage();
  return Storage.getInstance(jsObject);
}

/// Available options to pass to [initializeApp()]
///
/// See: <https://firebase.google.com/docs/reference/admin/node/admin.AppOptions>.
class AppOptions extends JsObjectWrapper<admin_interop.AppOptionsJsImpl>
    implements _AppOptions {
  static final _expando = Expando<AppOptions>();

  /// Available options to pass to [initializeApp()]
  ///
  /// See: <https://firebase.google.com/docs/reference/admin/node/admin.AppOptions>.
  /// credential :
  /// A [Credential] object used to authenticate the Admin SDK.
  ///
  /// `databaseAuthVariableOverride`:
  /// The object to use as the auth variable in your Realtime Database Rules
  /// when the Admin SDK reads from or writes to the Realtime Database. This
  /// allows you to downscope the Admin SDK from its default full read and
  /// write privileges.
  ///
  /// `databaseUrl`:
  /// The URL of the Realtime Database from which to read and write data.
  ///
  /// `serviceAccountId`:
  /// The ID of the service account to be used for signing custom tokens. This
  /// can be found in the client_email field of a service account JSON file.
  ///
  /// `storageBucket`:
  /// The name of the Google Cloud Storage bucket used for storing application
  /// data. Use only the bucket name without any prefixes or additions (do not
  /// prefix the name with "gs://").
  ///
  /// `projectId`:
  /// The ID of the Google Cloud project associated with the App.
  ///
  /// `httpAgent`:
  /// An [HttpAgent] to be used when making outgoing HTTP calls. This Agent
  /// instance is used by all services that make REST calls (e.g. auth,
  /// messaging, projectManagement).
  /// Realtime Database and Firestore use other means of communicating with the
  /// backend servers, so they do not use this HTTP Agent. Credential instances
  /// also do not use this HTTP Agent, but instead support specifying an HTTP
  /// Agent in the corresponding factory methods.
  factory AppOptions({
    Credential credential,
    Map<String, dynamic> databaseAuthVariableOverride,
    String databaseUrl,
    String serviceAccountId,
    String storageBucket,
    String projectId,
    HttpAgent httpAgent,
  }) =>
      AppOptions._fromJsObject(admin_interop.AppOptionsJsImpl(
        credential: credential?.jsObject,
        databaseAuthVariableOverride: jsify(databaseAuthVariableOverride),
        databaseURL: databaseUrl,
        serviceAccountId: serviceAccountId,
        storageBucket: storageBucket,
        projectId: projectId,
        httpAgent: httpAgent,
      ));

  AppOptions._fromJsObject(admin_interop.AppOptionsJsImpl jsObject)
      : super.fromJsObject(jsObject);

  @override
  Credential get credential => Credential.getInstance(jsObject.credential);
  @override
  set credential(Credential v) => jsObject.credential = v.jsObject;

  @override
  Map<String, dynamic> get databaseAuthVariableOverride =>
      dartify(jsObject.databaseAuthVariableOverride);
  @override
  set databaseAuthVariableOverride(Map<String, dynamic> v) =>
      jsObject.databaseAuthVariableOverride = jsify(v);

  @override
  String get databaseUrl => jsObject.databaseURL;
  @override
  set databaseUrl(String v) => jsObject.databaseURL = v;

  @override
  HttpAgent get httpAgent => jsObject.httpAgent;
  @override
  set httpAgent(HttpAgent v) => jsObject.httpAgent = v;

  @override
  String get projectId => jsObject.projectId;
  @override
  set projectId(String v) => jsObject.projectId = v;

  @override
  String get serviceAccountId => jsObject.serviceAccountId;
  @override
  set serviceAccountId(String v) => jsObject.serviceAccountId = v;

  @override
  String get storageBucket => jsObject.storageBucket;
  @override
  set storageBucket(String v) => jsObject.storageBucket = v;

  /// Get instance of [AppOPtions]
  static AppOptions getInstance(admin_interop.AppOptionsJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= AppOptions._fromJsObject(jsObject);
  }
}

abstract class _AppOptions {
  /// A [Credential] object used to authenticate the Admin SDK.
  Credential credential;

  /// The object to use as the auth variable in your Realtime Database Rules
  /// when the Admin SDK reads from or writes to the Realtime Database. This
  /// allows you to downscope the Admin SDK from its default full read and
  /// write privileges.
  Map<String, dynamic> databaseAuthVariableOverride;

  /// The URL of the Realtime Database from which to read and write data.
  String databaseUrl;

  /// An HTTP Agent to be used when making outgoing HTTP calls. This Agent
  /// instance is used by all services that make REST calls (e.g. auth,
  /// messaging, projectManagement).
  ///
  /// Realtime Database and Firestore use other means of communicating with the
  /// backend servers, so they do not use this HTTP Agent. Credential instances
  /// also do not use this HTTP Agent, but instead support specifying an HTTP
  /// Agent in the corresponding factory methods.
  HttpAgent httpAgent;

  /// The ID of the Google Cloud project associated with the App.
  String projectId;

  /// The ID of the service account to be used for signing custom tokens. This
  /// can be found in the client_email field of a service account JSON file.
  String serviceAccountId;

  ///The name of the Google Cloud Storage bucket used for storing application
  ///data. Use only the bucket name without any prefixes or additions (do not
  ///prefix the name with "gs://").
  String storageBucket;
}

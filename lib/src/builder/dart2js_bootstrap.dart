import 'dart:async';
import 'dart:convert';

import 'package:build/build.dart';
import 'package:build_modules/build_modules.dart';
import 'package:build_web_compilers/build_web_compilers.dart';
import 'package:node_preamble/preamble.dart';
import 'package:path/path.dart' as p;

const jsEntrypointExtension = '.dart.js';
const preambleEntrypointExtension = '.dart.node.js';
const jsEntrypointSourceMapExtension = '.dart.js.map';
const ddcBootstrapExtension = '.dart.bootstrap.js';
const jsEntrypointArchiveExtension = '.dart.js.tar.gz';
const digestsEntrypointExtension = '.digests';

/// Compiles an the primary input of [buildStep] with dart2js.
///
/// If [skipPlatformCheck] is `true` then all `dart:` imports will be
/// allowed in all packages.
Future<void> bootstrapDart2Js(
  BuildStep buildStep,
  List<String> dart2JsArgs,
  Map nodeModules, {
  bool skipPlatformCheck = false,
}) async {
  var dartEntrypointId = buildStep.inputId;
  var moduleId =
      dartEntrypointId.changeExtension(moduleExtension(dart2jsPlatform));
  var args = <String>[];
  {
    var module = Module.fromJson(
        json.decode(await buildStep.readAsString(moduleId))
            as Map<String, dynamic>);
    List<Module> allDeps;
    try {
      allDeps = (await module.computeTransitiveDependencies(buildStep,
          throwIfUnsupported: !skipPlatformCheck))
        ..add(module);
    } on UnsupportedModules catch (e) {
      var librariesString = (await e.exactLibraries(buildStep).toList())
          .map((lib) => AssetId(lib.id.package,
              lib.id.path.replaceFirst(moduleLibraryExtension, '.dart')))
          .join('\n');
      log.warning('''
Skipping compiling ${buildStep.inputId} with dart2js because some of its
transitive libraries have sdk dependencies that not supported on this platform:

$librariesString

https://github.com/dart-lang/build/blob/master/docs/faq.md#how-can-i-resolve-skipped-compiling-warnings
''');
      return;
    }

    var scratchSpace = await buildStep.fetchResource(scratchSpaceResource);
    var allSrcs = allDeps.expand((module) => module.sources);
    await scratchSpace.ensureAssets(allSrcs, buildStep);

    var dartPath = dartEntrypointId.path.startsWith('lib/')
        ? 'package:${dartEntrypointId.package}/'
            '${dartEntrypointId.path.substring('lib/'.length)}'
        : dartEntrypointId.path;
    var jsOutputPath =
        '${p.withoutExtension(dartPath.replaceFirst('package:', 'packages/'))}'
        '$jsEntrypointExtension';
    args = dart2JsArgs.toList()
      ..addAll([
        '--packages=${p.join('.dart_tool', 'package_config.json')}',
        '-o$jsOutputPath',
        dartPath,
      ]);
  }

  var dart2js = await buildStep.fetchResource(dart2JsWorkerResource);
  var result = await dart2js.compile(args);
  var jsOutputId = dartEntrypointId.changeExtension(jsEntrypointExtension);
  var jsOutputFile = scratchSpace.fileFor(jsOutputId);
  if (result.succeeded && await jsOutputFile.exists()) {
    log.info(result.output);

    final preambleId =
        dartEntrypointId.changeExtension(preambleEntrypointExtension);
    final jsSourceMapId =
        dartEntrypointId.changeExtension(jsEntrypointSourceMapExtension);
    final buffer = StringBuffer();
    String preamble;

    if (nodeModules != null) {
      nodeModules.forEach((key, value) {
        buffer.write('const $key = require("$value");');
      });
      final globals = nodeModules.keys.toList().cast<String>();
      preamble = getPreamble(minified: true, additionalGlobals: globals);
    } else {
      preamble = getPreamble(minified: true);
    }

    final contents = jsOutputFile.readAsStringSync();
    buffer..writeln(preamble)..writeln(contents);

    await buildStep.writeAsString(preambleId, buffer.toString());
    await scratchSpace.ensureAssets([preambleId], buildStep);
    await scratchSpace.copyOutput(jsOutputId, buildStep);
    await scratchSpace.copyOutput(preambleId, buildStep);
    if (await scratchSpace.fileFor(jsSourceMapId).exists()) {
      await scratchSpace.copyOutput(jsSourceMapId, buildStep);
    }
  } else {
    log.severe(result.output);
  }
}

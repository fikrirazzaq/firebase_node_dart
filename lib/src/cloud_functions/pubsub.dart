import 'package:js/js.dart';

import '../utils/js.dart';
import '../utils/utils.dart';
import 'functions.dart';
import 'interop/configuration_interop.dart';
import 'interop/pubsub_interop.dart' as interop;

class Message extends JsObjectWrapper<interop.MessageJsImpl> {
  Message._fromJsObject(interop.MessageJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///User-defined attributes published with the message, if any.
  Map<String, String> get attributes => jsObject.attributes == null
      ? null
      : dartifyMap(jsObject.attributes).cast<String, String>();

  ///The data payload of this message object as a base64-encoded string.
  String get data => jsObject.data;

  ///The JSON data payload of this message object, if any.
  Map<String, dynamic> get json =>
      jsObject.json == null ? null : dartifyMap(jsObject.json);
}

class PubsubFunctionsBuilder
    extends JsObjectWrapper<interop.PubsubFunctionsBuilderJsImpl> {
  PubsubFunctionsBuilder.fromJsObject(
      interop.PubsubFunctionsBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ScheduleBuilder schedule(String schedule) =>
      ScheduleBuilder.getInstance(jsObject.schedule(schedule));

  ///Registers a Cloud Function triggered when a Google Cloud Pub/Sub message
  ///is sent to a specified topic.
  TopicBuilder topic(String topic) =>
      TopicBuilder.getInstance(jsObject.topic(topic));
}

class ScheduleBuilder extends JsObjectWrapper<interop.ScheduleBuilderJsImpl> {
  static final _expando = Expando<ScheduleBuilder>();

  static ScheduleBuilder getInstance(interop.ScheduleBuilderJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= ScheduleBuilder._fromJsObject(jsObject);
  }

  ScheduleBuilder._fromJsObject(interop.ScheduleBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  dynamic onRun(dynamic Function(EventContext) handler) {
    return jsObject.onRun(allowInterop((contextJs) {
      final context = EventContext.fromJsObject(contextJs);
      dynamic _wrapper(_, EventContext context) => handler(context);
      return cloudFunction(null, context, _wrapper);
    }));
  }

  ScheduleBuilder retryConfig(ScheduleRetryConfig config) =>
      ScheduleBuilder.getInstance(jsObject.retryConfig(config));

  ScheduleBuilder timezone(String timezone) =>
      ScheduleBuilder.getInstance(jsObject.timezone(timezone));
}

class TopicBuilder extends JsObjectWrapper<interop.TopicBuilderJsImpl> {
  static final _expando = Expando<TopicBuilder>();

  static TopicBuilder getInstance(interop.TopicBuilderJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= TopicBuilder._fromJsObject(jsObject);
  }

  TopicBuilder._fromJsObject(interop.TopicBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Event handler that fires every time a Cloud Pub/Sub message is published.
  dynamic onPublish(CloudFunction<Message> handler) {
    return jsObject.onPublish(allowInterop((dataJs, contextJs) {
      final data = Message._fromJsObject(dataJs);
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(data, context, handler);
    }));
  }
}

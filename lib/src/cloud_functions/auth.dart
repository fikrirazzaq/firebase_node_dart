import 'package:js/js.dart';

import '../../admin.dart';
import '../utils/js.dart';
import 'functions.dart';
import 'interop/auth_interop.dart' as interop;

export 'interop/auth_interop.dart' show UserRecordMetadata;

///Construct auth functions
class AuthFunctionsBuilder
    extends JsObjectWrapper<interop.AuthFunctionsBuilderJsImpl> {
  ///Construct auth functions
  AuthFunctionsBuilder.fromJsObject(interop.AuthFunctionsBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Handle events related to Firebase authentication users.
  UserBuilder user() => UserBuilder.getInstance(jsObject.user());
}

///Builder used to create Cloud Functions for Firebase Auth user lifecycle
///events.
class UserBuilder extends JsObjectWrapper<interop.UserBuilderJsImpl> {
  static final _expando = Expando<UserBuilder>();

  ///Builder used to create Cloud Functions for Firebase Auth user lifecycle
  ///events.
  static UserBuilder getInstance(interop.UserBuilderJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= UserBuilder._fromJsObject(jsObject);
  }

  UserBuilder._fromJsObject(interop.UserBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Respond to the creation of a Firebase Auth user.
  dynamic onCreate(CloudFunction<UserRecord> handler) {
    return jsObject.onCreate(allowInterop((dataJs, contextJs) {
      return cloudFunction(
        UserRecord.getInstance(dataJs),
        EventContext.fromJsObject(contextJs),
        handler,
      );
    }));
  }

  ///Respond to the deletion of a Firebase Auth user.
  dynamic onDelete(CloudFunction<UserRecord> handler) {
    return jsObject.onDelete(allowInterop((dataJs, contextJs) {
      return cloudFunction(
        UserRecord.getInstance(dataJs),
        EventContext.fromJsObject(contextJs),
        handler,
      );
    }));
  }
}

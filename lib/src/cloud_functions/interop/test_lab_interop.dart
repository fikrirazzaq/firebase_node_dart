// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.functions.interop.test_lab;

import 'package:js/js.dart';

import 'functions_interop.dart';

@JS()
@anonymous
abstract class ClientInfoJsImpl {
  external dynamic /*Map<String,dynamic>*/ get details;
  external String get name;
}

@JS()
@anonymous
abstract class ResultStorage {
  external String get gcsPath;
  external String get resultsUrl;
  external String get toolResultsExecutionId;
  external String get toolResultsHistoryId;
}

@JS()
@anonymous
abstract class TestLabFunctionsBuilderJsImpl {
  external TestMatrixBuilderJsImpl testMatrix();
}

@JS()
@anonymous
abstract class TestMatrixBuilderJsImpl {
  external dynamic onComplete(JsHandler<TestMatrixJsImpl> handler);
}

@JS()
@anonymous
abstract class TestMatrixJsImpl {
  external ClientInfoJsImpl get clientInfo;

  external String get createTime;

  /// "DETAILS_UNAVAILABLE" | "MALFORMED_APK" | "MALFORMED_TEST_APK" |
  /// "NO_MANIFEST" | "NO_PACKAGE_NAME" | "INVALID_PACKAGE_NAME" |
  /// "TEST_SAME_AS_APP" | "NO_INSTRUMENTATION" | "NO_SIGNATURE" |
  /// "INSTRUMENTATION_ORCHESTRATOR_INCOMPATIBLE" | "NO_TEST_RUNNER_CLASS" |
  /// "NO_LAUNCHER_ACTIVITY" | "FORBIDDEN_PERMISSIONS" |
  /// "INVALID_ROBO_DIRECTIVES" | "INVALID_RESOURCE_NAME" |
  /// "INVALID_DIRECTIVE_ACTION" | "TEST_LOOP_INTENT_FILTER_NOT_FOUND" |
  /// "SCENARIO_LABEL_NOT_DECLARED" | "SCENARIO_LABEL_MALFORMED" |
  /// "SCENARIO_NOT_DECLARED" | "DEVICE_ADMIN_RECEIVER" |
  /// "MALFORMED_XC_TEST_ZIP" | "BUILT_FOR_IOS_SIMULATOR" |
  /// "NO_TESTS_IN_XC_TEST_ZIP" | "USE_DESTINATION_ARTIFACTS" |
  /// "TEST_NOT_APP_HOSTED" | "PLIST_CANNOT_BE_PARSED" | "NO_CODE_APK" |
  /// "INVALID_INPUT_APK" | "INVALID_APK_PREVIEW_SDK"
  external String get invalidMatrixDetails;

  /// "SUCCESS" | "FAILURE" | "INCONCLUSIVE" | "SKIPPED"
  external String get outcomeSummary;

  external ResultStorage get resultStorage;

  /// "VALIDATING" | "PENDING" | "FINISHED" | "ERROR" | "INVALID"
  external String get state;

  external String get testMatrixId;
}

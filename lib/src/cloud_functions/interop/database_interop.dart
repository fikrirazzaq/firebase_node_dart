// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.functions.interop.database;

import 'package:js/js.dart';

import '../../admin/interop/database_interop.dart' as admin;
import 'functions_interop.dart';

@JS()
@anonymous
abstract class DatabaseFunctionsBuilderJsImpl {
  ///Selects a database instance that will trigger the function. If omitted,
  ///will pick the default database for your project.
  external InstanceBuilderJsImpl instance([String instance]);

  ///Select Firebase Realtime Database Reference to listen to.
  external RefBuilderJsImpl ref(String path);
}

@JS()
@anonymous
abstract class InstanceBuilderJsImpl {
  ///Firebase Realtime Database reference builder interface.
  external RefBuilderJsImpl ref(String path);
}

@JS()
@anonymous
abstract class RefBuilderJsImpl {
  ///Event handler that fires every time new data is created in Firebase
  ///Realtime Database.
  external dynamic onCreate(JsHandler<admin.DataSnapshotJsImpl> handler);

  ///Event handler that fires every time data is deleted from Firebase Realtime
  ///Database.
  external dynamic onDelete(JsHandler<admin.DataSnapshotJsImpl> handler);

  ///Event handler that fires every time data is updated in Firebase Realtime
  ///Database.
  external dynamic onUpdate(
      JsHandler<ChangeJsImpl<admin.DataSnapshotJsImpl>> handler);

  ///Event handler that fires every time a Firebase Realtime Database write of
  ///any kind (creation, update, or delete) occurs.
  external dynamic onWrite(
      JsHandler<ChangeJsImpl<admin.DataSnapshotJsImpl>> handler);
}
